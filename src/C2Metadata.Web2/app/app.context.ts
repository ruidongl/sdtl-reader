﻿import { Component, Input, Output } from '@angular/core';
import { Services } from './services/services';

@Component({
    selector: 'my-context',
    template: `
        <div class="div4" *ngIf="(content | json) != '{}' && content[id] != null">
            <h3 class="title">Summary</h3>
            <div *ngFor="let key of objectKeys(content)">
                <p *ngIf="key != 'commands' && content[key]?.constructor.name != 'Array'">{{key}} : {{content[key]}}</p>
            </div>
            <br>
            <br>
            <br>
            <h3 *ngIf="content[messages] != null" class="emphasize">Errors</h3>
            <table>
                <tr id="table_header">
                    <th id="table_header_content">Severity</th>
                    <th id="table_header_content">Line</th>
                    <th id="table_header_content">Position</th>
                    <th id="table_header_content">Messages</th>
                </tr>
                <tr id="table_content" *ngFor="let item of content[messages]">
                    <td>
                        <div class="table-div">
                            <i *ngIf="item?.severity == 'Warning'" class="material-icons" style="float:left; font-size:20px;color:#CCCC00">warning</i>
                            <i *ngIf="item?.severity == 'Error'" class="material-icons" style="float:left; font-size:20px;color:red">error</i>
                            <p>{{item?.severity}}</p>
                        </div>
                    </td>
                    <td>{{item?.lineNumber}}</td>
                    <td>{{item?.characterPosition}}</td>
                    <td>{{item?.messageText}}</td>
                </tr>
            </table>
        </div>
        <div class="div4" *ngIf="(content | json) != '{}' && content[id] == null">
            <h3 class="title">Overview</h3>
            <p *ngIf="content[variable] != null">Variable: {{content[variable]}}</p>
            <p *ngIf="content[variableRange] != null">Variable Range: {{content[variableRange]}}</p>
            <p *ngIf="content[propertyName] != null">PropertyName: {{content[propertyName]}}</p>
            <p *ngIf="content[value] != null">Value: {{content[value]}}</p>
            <p *ngIf="content[fileName] != null">FileName: {{content[fileName]}}</p>
            <p *ngIf="content[variables] != null">Variables: {{content[variables]}}</p>
            <p *ngIf="content[renames] != null">Renames: oldName = {{content[renames][0][oldName]}} → newName= {{content[renames][0][newName]}}</p>
            <p *ngIf="content[variableName] != null">VariableName: {{content[variableName]}}</p>
            <p *ngIf="content[label] != null">Label: {{content[label]}}</p>
            <p *ngIf="content[condition] != null">Type: {{content[condition][type]}}</p>
            <p *ngIf="content[condition] != null">Label: {{content[condition][function]}}</p>
            <br>
            <p>LineNumberStart: {{content[sourceInformation]?.lineNumberStart}}</p>
            <p>LineNumberEnd: {{content[sourceInformation]?.lineNumberEnd}}</p>
            <p>OriginalSource: {{content[sourceInformation]?.originalSourceText}}</p>
            <br>
            <my-list [data]=content>Loading component</my-list>
        </div>
`
})
//<p * ngFor="let key of objectKeys(content)" > {{ key }} : <span * ngIf="content[key].constructor.name != 'Object'" > {{ content[key] }}</span>
//    < /p>
export class AppContext{
    content: object;

    id: string = "id";
    messages: string = "messages";

    variable: string = "variable";
    propertyName: string = "propertyName"
    value: string = "value";
    fileName: string = "fileName";
    variables: string = "variables";
    renames: string = "renames";
    oldName: string = "oldName";
    newName: string = "newName";
    variableName: string = "variableName";
    variableRange: string = "variableRange";
    label: string = "label";
    condition: string = "condition";
    type: string = "$type";
    function: string = "function";
    expression: string = "expression";
    sourceInformation: string = "sourceInformation";
    orginalSourceText: string = "originalSourceText";
    lineNumberStart: string = "lineNumberStart";
    lineNumberEnd: string = "lineNumberEnd";
    processedSource: string = "ProcessdSource";
    expression_res: string;
    objectKeys = Object.keys;
    constructor(private service: Services) {}

    ngOnInit() {
        this.service.curdata.subscribe(data => this.content= data);
    }
}