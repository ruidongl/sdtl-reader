"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
const services_1 = require("./services/services");
let ObjectList = class ObjectList {
    //info: any;
    //@Output() notify = new EventEmitter<any>();
    constructor(service) {
        this.service = service;
        //obj: Object = JSON.parse(this.data);;
        this.objectKeys = Object.keys;
    }
    isObj(val) {
        return typeof val === 'object';
    }
    onClick(info) {
        if (info != null) {
            this.service.sendData(info);
        }
    }
};
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], ObjectList.prototype, "data", void 0);
ObjectList = __decorate([
    core_1.Component({
        selector: 'my-obj',
        template: `
        <ul>
            <li *ngFor="let key of objectKeys(data)" class="list">
                <a (click)='onClick(data[key])'>{{key}}</a>
                <my-obj *ngIf="data[key].constructor.name == 'Object'" [data]="data[key]"></my-obj>
            </li>
        </ul>
`
    }),
    __metadata("design:paramtypes", [services_1.Services])
], ObjectList);
exports.ObjectList = ObjectList;
//# sourceMappingURL=objectlist.js.map