﻿import { Injectable } from '@angular/core';
import { BehaviorSubject } from '../../node_modules/rxjs/BehaviorSubject';


@Injectable()
export class Services {

    public newSubject = new BehaviorSubject<any>({});
    curdata = this.newSubject.asObservable();
    constructor() { }

    sendData(data: any) {
        this.newSubject.next(data);
    }
}