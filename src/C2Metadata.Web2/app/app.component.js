"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
const services_1 = require("./services/services");
let AppComponent = class AppComponent {
    constructor(elementRef, service) {
        this.elementRef = elementRef;
        this.service = service;
        //objectKeys = Object.keys();
        this.data = this.elementRef.nativeElement.getAttribute("data");
        this.obj = JSON.parse(this.data);
        this.commands = "commands";
        this.command = "command";
        this.objectKeys = Object.keys;
    }
    isObj(val) {
        return typeof val === 'object';
    }
    onClick(info) {
        if (info != null) {
            this.service.sendData(info);
        }
    }
};
AppComponent = __decorate([
    core_1.Component({
        selector: 'my-app',
        template: `
    <ul class="tree" *ngIf="data != null">
        <li class="list">
           <a (click)='onClick(obj)'>Summary</a><input type="checkbox">
        </li>
        <li class="list">
           <a>Content</a><input type="checkbox">
            <ul>
                <li *ngFor="let key of objectKeys(obj[commands])" class="list">
                    <a (click)='onClick(obj[commands][key])'>{{obj[commands][key][command]}}</a>
                </li>
            </ul>
        </li>
    </ul>`
    }),
    __metadata("design:paramtypes", [core_1.ElementRef, services_1.Services])
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map