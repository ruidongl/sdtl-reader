﻿import { Command } from './Command'

export interface Spss {
    id: string;
    sourceFileName: string;
    sourceLanguage: string;
    scriptMD5: string;
    scriptSHA1: string;
    sourceFileLastUpdate: string;
    sourceFileSize: Number;
    lineCount: Number;
    commandCount: Number;
    parser: string;
    parserVersion: string;
    modelVersion: string;
    modelCreatedTime: string;
    commands: Command[];
}