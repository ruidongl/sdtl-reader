﻿import { SourceInfo } from './SourceInfo';
import { Expression } from './Expression';

export interface Command {
    command: string;
    sourceInformation: SourceInfo[];
    variable: string;
    expression: Expression[];
}