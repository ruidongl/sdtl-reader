﻿$(document).ready(function () {
    //change the text of upload button
    $('#file-upload').change(function () {
        var i = $(this).prev('label').clone();
        var file = $('#file-upload')[0].files[0].name;
        $(this).prev('label').text(file);
    });

    $('#json-upload').change(function () {
        var i = $(this).prev('label').clone();
        var file = $('#json-upload')[0].files[0].name;
        $(this).prev('label').text(file);
    });

    //expanding the nested json structure
    $('.node').click(function (event) {
        var $e = $(this),
            $childrenWrapper = $e.find('.childrenWrapper'),
            $children = $e.find('.children'),
            h = $e.is('.expanded') ? 0 : $children.height(); // target height depending on it being expanded or not.
        $childrenWrapper.height(h); // applying height (see css transition on .node > .childrenWrapper)
        $(this).toggleClass('expanded');
    });

    document.getElementById('file-upload').addEventListener('change', getFile)
    document.getElementById('json-upload').addEventListener('change', getFile)

    function getFile(event) {
        const input = event.target
        if ('files' in input && input.files.length > 0) {
            placeFileContent(
                document.getElementById('fileContent'),
                input.files[0])
        }
    }

    function placeFileContent(target, file) {
        readFileContent(file).then(content => {
            target.value = content
        }).catch(error => console.log(error))
    }

    function readFileContent(file) {
        const reader = new FileReader()
        return new Promise((resolve, reject) => {
            reader.onload = event => resolve(event.target.result)
            reader.onerror = error => reject(error)
            reader.readAsText(file)
        })
    }

    var popup = document.getElementById("popmodal");
    var link = document.getElementById("linkToPopup");

    var close = document.getElementsByClassName("close")[0];
    openPopup = function () {
        popup.style.display = "block";
    }

    close.addEventListener("click", function () {
        popup.style.display = "none";
        return false;
    });

    window.onclick = function (event) {
        if (event.target == popup) {
            popup.style.display = "none";
        }
    }

});