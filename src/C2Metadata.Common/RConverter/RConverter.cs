﻿using Antlr4.Runtime;
using C2Metadata.Common.Model;
using C2Metadata.Common.Utility;
using C2Metadata.RToSdtl.Grammar;
using sdtl;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace C2Metadata.Common.RConverter
{
    public class RConverter
    {
        private string fileName;
        private bool isVerbose;

        public RConverter()
        {

        }

        public Program ConvertFile(string fileName, bool isVerbose = false)
        {
            this.fileName = fileName;
            this.isVerbose = isVerbose;

            string content = File.ReadAllText(fileName);

            Program program = Convert(content);
            AddGeneralProgramInformation(program, content);

            program.SourceFileName = Path.GetFileName(fileName);
            var fileInfo = new FileInfo(fileName);
            program.SourceFileLastUpdate = fileInfo.LastWriteTimeUtc;
            program.SourceFileSize = fileInfo.Length;

            return program;
        }


        public Program ConvertString(string content, bool isVerbose = false)
        {
            this.isVerbose = isVerbose;
            Program program = Convert(content);
            AddGeneralProgramInformation(program, content);

            program.SourceFileName = string.Empty;
            program.SourceFileLastUpdate = TimeHelper.GetUtcNowWithReasonablePrecision();
            program.SourceFileSize = content.Length;

            return program;
        }

        private Program Convert(string content)
        {
            Program program = new Program();
            program.SourceLanguage = "r";

            using (var reader = new MemoryStream(Encoding.UTF8.GetBytes(content ?? "")))
            {
                try
                {
                    // Tokenize and parse.
                    var inputStream = new AntlrInputStream(reader);
                    var lexer = new RLexer(inputStream);
                    var tokens = new CommonTokenStream(lexer);

                    var filter = new RFilter(tokens);
                    filter.stream();
                    tokens.Reset();

                    var parser = new RParser(tokens);
                    parser.BuildParseTree = true;

                    var errorListener = new MessageTrackingErrorListener();
                    parser.AddErrorListener(errorListener);

                    var tree = parser.prog();
                    string test = tree.ToStringTree();

                    // Add messages from the parser.
                    foreach (var message in errorListener.Messages)
                    {
                        program.Messages.Add(message);
                    }

                    // Visit everything in tree to build the commands.
                    var commandVisitor = new RCommandVisitor();
                    commandVisitor.SourceFileName = fileName;
                    var commandList = commandVisitor.VisitProg(tree)
                        as CommandList;

                    // Add messages from the tree visitor.
                    foreach (var message in commandVisitor.Messages)
                    {
                        program.Messages.Add(message);
                    }

                    program.Commands.AddRange(commandList.Commands);

                    // Very verbose output, if requested.
                    if (isVerbose)
                    {
                        Console.WriteLine("Tokens");
                        Console.WriteLine("----------");
                        foreach (var token in tokens.GetTokens())
                        {
                            Console.WriteLine(token.ToString());
                        }

                        Console.WriteLine("Tree");
                        Console.WriteLine("----------");
                        foreach (var kid in tree.children)
                        {
                            Console.WriteLine(kid.ToStringTree(parser));
                        }
                    }

                    return program;
                }
                catch (Exception ex)
                {
                    program.Messages.Add(new Message
                    {
                        Severity = "Error",
                        MessageText = "Unhandled error. " + ex.Message
                    });

                    return program;
                }
            }
        }

        private void AddGeneralProgramInformation(Program program, string content)
        {
            if (program.Commands != null)
            {
                program.CommandCount = program.Commands.Count;
            }

            program.LineCount = content.Split('\n').Length;
            program.ModelCreatedTime = TimeHelper.GetUtcNowWithReasonablePrecision();
            program.ModelVersion = VersionInformation.Version;
            program.Parser = "r-to-sdtl";
            program.ParserVersion = VersionInformation.Version;
            program.ScriptMD5 = HashHelper.GetMd5Hash(content);
            program.ScriptSHA1 = HashHelper.GetSha1Hash(content);
        }

    }
}
