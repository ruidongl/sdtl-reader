﻿using Antlr4.Runtime;
using Antlr4.Runtime.Misc;
using Antlr4.Runtime.Tree;
using C2Metadata.Common.Model;
using C2Metadata.RToSdtl.Grammar;
using sdtl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C2Metadata.Common.RConverter
{
    public class RCommandVisitor : RBaseVisitor<TransformBase>
    {
        public string SourceFileName { get; internal set; }

        public List<Message> Messages { get; } = new List<Message>();

        public override TransformBase VisitProg([NotNull] RParser.ProgContext context)
        {
            return base.VisitProg(context);
        }

        public override TransformBase VisitChildren(IRuleNode node)
        {
            var commandList = new CommandList();

            if (node == null)
            {
                return commandList;
            }

            int n = node.ChildCount;
            for (int i = 0; i < n; i++)
            {
                if (!ShouldVisitNextChild(node, commandList))
                {
                    break;
                }

                var c = node.GetChild(i);
                var childResult = c.Accept(this);
                AggregateResult(commandList, childResult);
            }

            return commandList;
        }

        protected override TransformBase AggregateResult(TransformBase aggregate, TransformBase nextResult)
        {
            if (nextResult == null)
            {
                return aggregate;
            }

            var mainList = aggregate as CommandList;
            if (mainList != null)
            {
                var newList = nextResult as CommandList;
                if (newList != null)
                {
                    foreach (var child in newList.Commands)
                    {
                        if (child != null)
                        {
                            mainList.Commands.Add(child);
                        }
                    }
                }
                else
                {
                    mainList.Commands.Add(nextResult);
                }
            }

            return mainList;
        }

        public override TransformBase VisitCallFunctionExpression([NotNull] RParser.CallFunctionExpressionContext context)
        {
            string[] analysisCommands = new string[]
            {
                "dim",
                "str",
                "head",
                "describeBy"
            };

            string rFunctionName = context.functionName.GetText();

            if (analysisCommands.Contains(rFunctionName))
            {
                return HandleUnsupportedCommand(context, true);
            }

            if (rFunctionName == "mutate")
            {
                return HandleMutate(context.argumentList, context);
            }

            return HandleUnsupportedCommand(context, false);
        }

        private TransformBase HandleMutate(RParser.SublistContext argumentList, RParser.CallFunctionExpressionContext context)
        {
            if (argumentList._argument.Count < 2)
            {
                return null;
            }

            // The first argument should be an ExpressionArgument  with an IdExpression.
            if (!(argumentList._argument[0] is RParser.ExpressionArgumentContext datasetArg))
            {
                return null;
            }

            string datasetName = datasetArg.GetText();

            // The remaining arguments create new variables.
            var assignmentArguments = argumentList._argument
                .Skip(1)
                .OfType<RParser.AssignmentArgumentContext>()
                .ToList();
            if (assignmentArguments == null)
            {
                return null;
            }

            var commandList = new CommandList();
            foreach (RParser.AssignmentArgumentContext assignment in assignmentArguments)
            {
                string variableName = assignment.variableName.Text;

                var compute = new Compute();
                // TODO Mark the dataset to which this computation applies.
                compute.Variable = new VariableSymbolExpression { VariableName = variableName };
                compute.Expression = ParseExpression(assignment.assignmentExpr);
                RecordSourceInformation(compute, context);

                commandList.Commands.Add(compute);
            }

            return commandList;
        }

        private ExpressionBase ParseExpression(RParser.ExprContext expression)
        {
            if (expression is RParser.AddExpressionContext addition)
            {
                var result = new FunctionCallExpression();
                result.Function = "addition";
                result.IsSdtlName = true;
                HandleArgumentsForFunctionCallTransformation(addition.expr(), result, "addition");
                return result;
            }
            else if (expression is RParser.SubtractExpressionContext subtract)
            {
                var result = new FunctionCallExpression();
                result.Function = "subtraction";
                result.IsSdtlName = true;
                HandleArgumentsForFunctionCallTransformation(subtract.expr(), result, "subtraction");
                return result;
            }
            else if (expression is RParser.DivideExpressionContext divide)
            {
                var result = new FunctionCallExpression();
                result.Function = "division";
                result.IsSdtlName = true;
                HandleArgumentsForFunctionCallTransformation(divide.expr(), result, "division");
                return result;
            }
            else if (expression is RParser.MultiplyExpressionContext multiply)
            {
                var result = new FunctionCallExpression();
                result.Function = "multiplication";
                result.IsSdtlName = true;
                HandleArgumentsForFunctionCallTransformation(multiply.expr(), result, "multiplication");
                return result;
            }
            else if (expression is RParser.ExponentExpressionContext exponent)
            {
                var result = new FunctionCallExpression();
                result.Function = "exponentiation";
                result.IsSdtlName = true;
                HandleArgumentsForFunctionCallTransformation(exponent.expr(), result, "exponentiation");
                return result;
            }
            else if (expression is RParser.IdExpressionContext id)
            {
                return new VariableSymbolExpression { VariableName = id.GetText() };
            }
            else if (expression is RParser.IntExpressionContext intExpr)
            {
                return new NumericConstantExpression { NumericType = "Integer", Value = intExpr.GetText() };
            }



            return null;
        }

        private void HandleArgumentsForFunctionCallTransformation(RParser.ExprContext[] expressionTree, FunctionCallExpression expr, string expressionType)
        {
            if (expressionTree.Length > 0)
            {
                var arg = new FunctionArgument();
                arg.ArgumentValue = ParseExpression(expressionTree[0]);
                expr.Arguments.Add(arg);
            }
            if (expressionTree.Length > 1)
            {
                var arg = new FunctionArgument();
                arg.ArgumentValue = ParseExpression(expressionTree[1]);
                expr.Arguments.Add(arg);
            }

            if (expressionTree.Length < 2)
            {
                Messages.Add(new Message
                {
                    Severity = "Warning",
                    MessageText = $"{expressionType} expression: cannot detect expressions"
                });
            }
        }

        public override TransformBase VisitDefineFunctionExpression([NotNull] RParser.DefineFunctionExpressionContext context)
        {
            return HandleUnsupportedCommand(context, false);
        }

        public override TransformBase VisitCompoundExpression([NotNull] RParser.CompoundExpressionContext context)
        {
            return HandleUnsupportedCommand(context, false);
        }

        public override TransformBase VisitAssignmentExpression([NotNull] RParser.AssignmentExpressionContext context)
        {
            return HandleUnsupportedCommand(context, false);
        }

        public override TransformBase VisitIfExpression([NotNull] RParser.IfExpressionContext context)
        {
            return HandleUnsupportedCommand(context, false);
        }

        public override TransformBase VisitIfElseExpression([NotNull] RParser.IfElseExpressionContext context)
        {
            return HandleUnsupportedCommand(context, false);
        }

        public override TransformBase VisitForExpression([NotNull] RParser.ForExpressionContext context)
        {
            return HandleUnsupportedCommand(context, false);
        }

        public override TransformBase VisitWhileExpression([NotNull] RParser.WhileExpressionContext context)
        {
            return HandleUnsupportedCommand(context, false);
        }

        public override TransformBase VisitRepeatExpression([NotNull] RParser.RepeatExpressionContext context)
        {
            return HandleUnsupportedCommand(context, false);
        }

        public override TransformBase VisitGetHelpExpression([NotNull] RParser.GetHelpExpressionContext context)
        {
            return HandleUnsupportedCommand(context, false);
        }

        public override TransformBase VisitParenthesizedExpression([NotNull] RParser.ParenthesizedExpressionContext context)
        {
            return HandleUnsupportedCommand(context, false);
        }

        public override TransformBase VisitTerminal(ITerminalNode node)
        {
            if (node.Payload is CommonToken commonToken)
            {
                if (commonToken.Type == RParser.COMMENT)
                {
                    return HandleComment(node);
                }
            }
            return base.VisitTerminal(node);
        }

        private TransformBase HandleComment(ITerminalNode node)
        {
            var result = new Comment();
            result.Command = "comment";

            string text = node.GetText().Trim();
            if (text.StartsWith("#"))
            {
                text = text.Substring(1).Trim();
            }

            result.CommentText = text;

            RecordSourceInformation(result, node);
            //RecordSourceInformation(result, node.Parent.RuleContext.Par);
            return result;
        }

        private void RecordSourceInformation(TransformBase command, ParserRuleContext context)
        {
            command.SourceInformation = new SourceInformation();
            command.SourceInformation.SourceStartIndex = context.Start.StartIndex;
            command.SourceInformation.SourceStopIndex = context.Stop.StopIndex;
            command.SourceInformation.LineNumberStart = context.Start.Line - 1;
            command.SourceInformation.LineNumberEnd = context.Stop.Line - 1;
            command.SourceInformation.OriginalSourceText = context.Start.InputStream
                .ToString()
                .Substring(context.Start.StartIndex, context.Stop.StopIndex - context.Start.StartIndex + 1)
                .Trim();
        }

        private void RecordSourceInformation(TransformBase command, ITerminalNode node)
        {
            var token = node.Payload as CommonToken;

            command.SourceInformation = new SourceInformation();
            command.SourceInformation.SourceStartIndex = token.StartIndex;
            command.SourceInformation.SourceStopIndex = token.StopIndex;
            command.SourceInformation.LineNumberStart = token.Line - 1;
            //command.SourceInformation.LineNumberEnd = context.Stop.Line - 1;
            command.SourceInformation.OriginalSourceText = token.InputStream
                .ToString()
                .Substring(token.StartIndex, token.StopIndex - token.StartIndex + 1)
                .Trim();
        }

        private TransformBase HandleUnsupportedCommand(Antlr4.Runtime.ParserRuleContext context, bool isAnalysis)
        {
            if (isAnalysis)
            {
                var result = new Analysis();
                RecordSourceInformation(result, context);
                return result;
            }
            else
            {
                var result = new Unsupported();
                result.Message = "This command is not yet supported.";
                RecordSourceInformation(result, context);
                return result;
            }
        }

    }
}
