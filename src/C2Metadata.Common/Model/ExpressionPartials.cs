﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sdtl
{
    public partial class StringConstantExpression
    {
        public override string ToString()
        {
            return Value;
        }
    }


    public partial class NumericConstantExpression
    {
        public override string ToString()
        {
            return Value;
        }
    }

    public partial class VariableSymbolExpression
    {
        public override string ToString()
        {
            return VariableName;
        }
    }

    public partial class VariableRangeExpression
    {
        public override string ToString()
        {
            return $"{First}..{Last}";
        }
    }

    public partial class FunctionCallExpression
    {
        public override string ToString()
        {
            string paramStr = string.Join(", ", Arguments);
            return $"{Name}({paramStr})";
        }
    }

    public partial class GroupedExpression
    {
        public string Function { get; set; } = "parentheses";

        public override string ToString()
        {
            return $"({Expression})";
        }
    }


}
