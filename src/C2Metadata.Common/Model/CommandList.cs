﻿using sdtl;
using System;
using System.Collections.Generic;
using System.Text;

namespace C2Metadata.Common.Model
{
    public class CommandList : TransformBase
    {
        public List<TransformBase> Commands { get; } = new List<TransformBase>();
    }
}
