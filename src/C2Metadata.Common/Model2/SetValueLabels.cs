using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Describes the assignment of labels to categorical values.
    /// 
    /// <summary>
    public partial class SetValueLabels : TransformBase
    {
        /// <summary>
        /// The variables to which a label will be assigned
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<VariableReferenceBase> Variables { get; set; } = new List<VariableReferenceBase>();
        public bool ShouldSerializeVariables() { return Variables.Count > 0; }
        /// <summary>
        /// The label to be assigned to the variable
        /// <summary>
        public List<ValueLabel> Labels { get; set; } = new List<ValueLabel>();
        public bool ShouldSerializeLabels() { return Labels.Count > 0; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("TransformBase").Descendants())
            {
                xEl.Add(el);
            }
            if (Variables != null && Variables.Count > 0)
            {
                foreach (var item in Variables)
                {
                    xEl.Add(item.ToXml("Variables"));
                }
            }
            if (Labels != null && Labels.Count > 0)
            {
                foreach (var item in Labels)
                {
                    xEl.Add(item.ToXml("Labels"));
                }
            }
            return xEl;
        }
    }
}

