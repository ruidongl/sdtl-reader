using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Describes the way in which rows are sorted.
    /// 
    /// <summary>
    public partial class SortCases : TransformBase
    {
        /// <summary>
        /// Describes how cases are sorted.
        /// <summary>
        public List<SortCriterion> SortCriteria { get; set; } = new List<SortCriterion>();
        public bool ShouldSerializeSortCriteria() { return SortCriteria.Count > 0; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("TransformBase").Descendants())
            {
                xEl.Add(el);
            }
            if (SortCriteria != null && SortCriteria.Count > 0)
            {
                foreach (var item in SortCriteria)
                {
                    xEl.Add(item.ToXml("SortCriteria"));
                }
            }
            return xEl;
        }
    }
}

