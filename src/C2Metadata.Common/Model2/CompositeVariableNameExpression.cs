using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// A composite variable name is used to describe a variable name that is computed.
    /// 
    /// <summary>
    public partial class CompositeVariableNameExpression : VariableReferenceBase
    {
        /// <summary>
        /// An expression that evaluates to the constructed variable name.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public ExpressionBase ComputedVariableName { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("VariableReferenceBase").Descendants())
            {
                xEl.Add(el);
            }
            if (ComputedVariableName != null) { xEl.Add(ComputedVariableName.ToXml("ComputedVariableName")); }
            return xEl;
        }
    }
}

