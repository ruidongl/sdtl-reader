using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Defines values that are treated as missing values for a list of variables.
    /// 
    /// <summary>
    public partial class SetMissingValues : TransformBase
    {
        /// <summary>
        /// The list of variables to which the missing values are assigned
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<VariableReferenceBase> Variables { get; set; } = new List<VariableReferenceBase>();
        public bool ShouldSerializeVariables() { return Variables.Count > 0; }
        /// <summary>
        /// The values to be considered as missing values
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<ExpressionBase> Values { get; set; } = new List<ExpressionBase>();
        public bool ShouldSerializeValues() { return Values.Count > 0; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("TransformBase").Descendants())
            {
                xEl.Add(el);
            }
            if (Variables != null && Variables.Count > 0)
            {
                foreach (var item in Variables)
                {
                    xEl.Add(item.ToXml("Variables"));
                }
            }
            if (Values != null && Values.Count > 0)
            {
                foreach (var item in Values)
                {
                    xEl.Add(item.ToXml("Values"));
                }
            }
            return xEl;
        }
    }
}

