using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Describes a criterion by which cases are sorted, including the variable name and whether to sort ascending or descending.
    /// 
    /// <summary>
    public partial class SortCriterion
    {
        /// <summary>
        /// The variable used to sort.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public VariableReferenceBase Variable { get; set; }
        /// <summary>
        /// The direction in which to sort.
        /// <summary>
        [StringValidation(new string[] {
            "Ascending"
,             "Descending"
        })]
        public string SortDirection { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public virtual XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            if (Variable != null) { xEl.Add(Variable.ToXml("Variable")); }
            if (SortDirection != null)
            {
                xEl.Add(new XElement(ns + "SortDirection", SortDirection));
            }
            return xEl;
        }
    }
}

