using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Describes files used in a MergeData command.
    /// 
    /// <summary>
    public partial class MergeFileDescription
    {

        /// <summary>
        /// Set the TypeDescriminator
        /// <summary>
        public MergeFileDescription() { this.TypeDescriminator = this.GetType().Name; }

        /// <summary>
        /// Type descriminator for json serialization
        /// <summary>
        [JsonProperty("$type")]
        public string TypeDescriminator { get; set; }

        /// <summary>
        /// Name of the file being merged.  May be "Active file".
        /// <summary>
        public string FileName { get; set; }
        /// <summary>
        /// Describes the type of merge performed. 
        /// <summary>
        [StringValidation(new string[] {
            "Sequential"
,             "OneToOne"
,             "ManyToOne"
,             "OneToMany"
,             "ManyToMany"
,             "OneToOne"
,             "Duplicates"
,             "Unmatched"
        })]
        public string MergeType { get; set; }
        /// <summary>
        /// Creates a new variable indicating whether the row came from this file or a different input file.
        /// <summary>
        public string MergeFlagVariable { get; set; }
        /// <summary>
        /// Variables to be renamed
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<RenamePair> RenameVariables { get; set; } = new List<RenamePair>();
        public bool ShouldSerializeRenameVariables() { return RenameVariables.Count > 0; }
        /// <summary>
        /// Outcome when the same variables exist in more than one file. "Master" means the value is taken from this file.  "Merge" means the value is taken from the other file.  "Update" means that value is taken from the other file when the value in this file is missing.
        /// <summary>
        [StringValidation(new string[] {
            "Master"
,             "Merge"
,             "Update"
        })]
        public string Overlap { get; set; }
        /// <summary>
        /// Generate new row when not matched to other files
        /// <summary>
        public bool NewRow { get; set; }
        /// <summary>
        /// List of variables to keep
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public VariableReferenceBase KeepVariables { get; set; }
        /// <summary>
        /// List of variables to drop
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public VariableReferenceBase DropVariables { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public virtual XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            if (FileName != null)
            {
                xEl.Add(new XElement(ns + "FileName", FileName));
            }
            if (MergeType != null)
            {
                xEl.Add(new XElement(ns + "MergeType", MergeType));
            }
            if (MergeFlagVariable != null)
            {
                xEl.Add(new XElement(ns + "MergeFlagVariable", MergeFlagVariable));
            }
            if (RenameVariables != null && RenameVariables.Count > 0)
            {
                foreach (var item in RenameVariables)
                {
                    xEl.Add(item.ToXml("RenameVariables"));
                }
            }
            if (Overlap != null)
            {
                xEl.Add(new XElement(ns + "Overlap", Overlap));
            }
            xEl.Add(new XElement(ns + "NewRow", NewRow));
            if (KeepVariables != null) { xEl.Add(KeepVariables.ToXml("KeepVariables")); }
            if (DropVariables != null) { xEl.Add(DropVariables.ToXml("DropVariables")); }
            return xEl;
        }
    }
}

