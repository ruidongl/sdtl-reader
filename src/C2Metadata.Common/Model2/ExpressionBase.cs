using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// TODO
    /// 
    /// <summary>
    public abstract partial class ExpressionBase
    {

        /// <summary>
        /// Set the TypeDescriminator
        /// <summary>
        public ExpressionBase() { this.TypeDescriminator = this.GetType().Name; }

        /// <summary>
        /// Type descriminator for json serialization
        /// <summary>
        [JsonProperty("$type")]
        public string TypeDescriminator { get; set; }

        /// <summary>
        /// The name of the argument, when the expression is passed to a function.
        /// <summary>
        public string Name { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public virtual XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            if (Name != null)
            {
                xEl.Add(new XElement(ns + "Name", Name));
            }
            return xEl;
        }
    }
}

