using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// ReshapeWide is not supported in the current version of SDTL, because it depends on values in the data.  However, it may be useful when values of the index variable are available in the metadata file or the data can be processed.
    /// 
    /// <summary>
    public partial class ReshapeWide : TransformBase
    {
        /// <summary>
        /// New variables created by this command.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<ReshapeItemDescription> MakeItems { get; set; } = new List<ReshapeItemDescription>();
        public bool ShouldSerializeMakeItems() { return MakeItems.Count > 0; }
        /// <summary>
        /// One or more variables identifying unique rows in the wide data.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public VariableReferenceBase IDVariables { get; set; }
        /// <summary>
        /// Variables to be dropped from the new dataset.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public VariableReferenceBase DropVariables { get; set; }
        /// <summary>
        /// Variables to be kept in the new dataset.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public VariableReferenceBase KeepVariables { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("TransformBase").Descendants())
            {
                xEl.Add(el);
            }
            if (MakeItems != null && MakeItems.Count > 0)
            {
                foreach (var item in MakeItems)
                {
                    xEl.Add(item.ToXml("MakeItems"));
                }
            }
            if (IDVariables != null) { xEl.Add(IDVariables.ToXml("IDVariables")); }
            if (DropVariables != null) { xEl.Add(DropVariables.ToXml("DropVariables")); }
            if (KeepVariables != null) { xEl.Add(KeepVariables.ToXml("KeepVariables")); }
            return xEl;
        }
    }
}

