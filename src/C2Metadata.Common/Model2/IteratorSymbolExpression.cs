using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// The name of an iterator symbol used as an index in describing the actions of 
    /// a loop.
    /// 
    /// <summary>
    public partial class IteratorSymbolExpression : ExpressionBase
    {
        /// <summary>
        /// A name used for the index variable that takes different values inside a loop.
        /// <summary>
        public string Name { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("ExpressionBase").Descendants())
            {
                xEl.Add(el);
            }
            if (Name != null)
            {
                xEl.Add(new XElement(ns + "Name", Name));
            }
            return xEl;
        }
    }
}

