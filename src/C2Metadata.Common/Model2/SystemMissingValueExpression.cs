using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Represents the a missing value in a statistical system.
    /// 
    /// <summary>
    public partial class SystemMissingValueExpression : ExpressionBase
    {
        /// <summary>
        /// The symbol used to represent the missing value.
        /// <summary>
        public string MissingValueSymbol { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("ExpressionBase").Descendants())
            {
                xEl.Add(el);
            }
            if (MissingValueSymbol != null)
            {
                xEl.Add(new XElement(ns + "MissingValueSymbol", MissingValueSymbol));
            }
            return xEl;
        }
    }
}

