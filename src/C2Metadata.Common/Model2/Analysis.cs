using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Describes an analysis command. An analysis command does not result in any data transformation.
    /// 
    /// <summary>
    public partial class Analysis : TransformBase
    {
        /// <summary>
        /// A message listing commands that are not supported in SDTL.
        /// <summary>
        public string Message { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("TransformBase").Descendants())
            {
                xEl.Add(el);
            }
            if (Message != null)
            {
                xEl.Add(new XElement(ns + "Message", Message));
            }
            return xEl;
        }
    }
}

