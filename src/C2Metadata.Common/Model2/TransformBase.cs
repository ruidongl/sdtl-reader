using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// TransformBase defines general properties available on all transform commands.
    /// 
    /// <summary>
    public abstract partial class TransformBase
    {

        /// <summary>
        /// Set the TypeDescriminator
        /// <summary>
        public TransformBase() { this.TypeDescriminator = this.GetType().Name; }

        /// <summary>
        /// Type descriminator for json serialization
        /// <summary>
        [JsonProperty("$type")]
        public string TypeDescriminator { get; set; }

        /// <summary>
        /// The type of transform command
        /// <summary>
        public string Command { get; set; }
        /// <summary>
        /// Information about the source of the transform command.
        /// <summary>
        public SourceInformation SourceInformation { get; set; }
        /// <summary>
        /// Indicates that the transform is capable of changing the values in the data.
        /// <summary>
        public bool CanChangeData { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public virtual XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            if (Command != null)
            {
                xEl.Add(new XElement(ns + "Command", Command));
            }
            if (SourceInformation != null) { xEl.Add(SourceInformation.ToXml("SourceInformation")); }
            xEl.Add(new XElement(ns + "CanChangeData", CanChangeData));
            return xEl;
        }
    }
}

