using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// An aggregation summarizes data using aggregation functions applied to data that may be grouped
    /// by one or more variables. The resulting summary data is added to each row of the
    /// existing dataset.
    /// 
    /// <summary>
    public partial class Aggregate : TransformBase
    {
        /// <summary>
        /// Variables used as keys to identify groups.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<VariableReferenceBase> GroupByVariables { get; set; } = new List<VariableReferenceBase>();
        public bool ShouldSerializeGroupByVariables() { return GroupByVariables.Count > 0; }
        /// <summary>
        /// The expressions that compute the aggregations. An aggregation function should be used.
        /// <summary>
        public List<Compute> AggregateVariables { get; set; } = new List<Compute>();
        public bool ShouldSerializeAggregateVariables() { return AggregateVariables.Count > 0; }
        /// <summary>
        /// The variable used as a weight in the operation.
        /// <summary>
        public VariableSymbolExpression WeightVariable { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("TransformBase").Descendants())
            {
                xEl.Add(el);
            }
            if (GroupByVariables != null && GroupByVariables.Count > 0)
            {
                foreach (var item in GroupByVariables)
                {
                    xEl.Add(item.ToXml("GroupByVariables"));
                }
            }
            if (AggregateVariables != null && AggregateVariables.Count > 0)
            {
                foreach (var item in AggregateVariables)
                {
                    xEl.Add(item.ToXml("AggregateVariables"));
                }
            }
            if (WeightVariable != null) { xEl.Add(WeightVariable.ToXml("WeightVariable")); }
            return xEl;
        }
    }
}

