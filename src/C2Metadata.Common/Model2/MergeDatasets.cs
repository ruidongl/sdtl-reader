using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Merges data holding overlapping cases but different variables.
    /// 
    /// <summary>
    public partial class MergeDatasets : TransformBase
    {
        /// <summary>
        /// Description of files to be merged.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<MergeFileDescription> MergeFiles { get; set; } = new List<MergeFileDescription>();
        public bool ShouldSerializeMergeFiles() { return MergeFiles.Count > 0; }
        /// <summary>
        /// A variable or list of variables that acts as the unique case identifier across datasets.  If MatchByVariables is absent, MergeType must be "sequential" on all files.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public VariableReferenceBase MergeByVariables { get; set; }
        /// <summary>
        /// The name of a variable set to 1 for the first row of each group of cases with the same value for the MatchByVariables variables and set to  0 for all other rows.
        /// <summary>
        public string FirstVariable { get; set; }
        /// <summary>
        /// The name of a variable set to 1 for the last row of each group of cases with the same value for the MatchByVariables variables and set to  0 for all other rows.
        /// <summary>
        public string LastVariable { get; set; }
        /// <summary>
        /// List of variables to keep
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public VariableReferenceBase KeepVariables { get; set; }
        /// <summary>
        /// List of variables to drop
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public VariableReferenceBase DropVariables { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("TransformBase").Descendants())
            {
                xEl.Add(el);
            }
            if (MergeFiles != null && MergeFiles.Count > 0)
            {
                foreach (var item in MergeFiles)
                {
                    xEl.Add(item.ToXml("MergeFiles"));
                }
            }
            if (MergeByVariables != null) { xEl.Add(MergeByVariables.ToXml("MergeByVariables")); }
            if (FirstVariable != null)
            {
                xEl.Add(new XElement(ns + "FirstVariable", FirstVariable));
            }
            if (LastVariable != null)
            {
                xEl.Add(new XElement(ns + "LastVariable", LastVariable));
            }
            if (KeepVariables != null) { xEl.Add(KeepVariables.ToXml("KeepVariables")); }
            if (DropVariables != null) { xEl.Add(DropVariables.ToXml("DropVariables")); }
            return xEl;
        }
    }
}

