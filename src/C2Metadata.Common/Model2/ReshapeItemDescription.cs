using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Descripbes a new variable created by reshaping a dataset from wide to long.
    /// 
    /// <summary>
    public partial class ReshapeItemDescription
    {

        /// <summary>
        /// Set the TypeDescriminator
        /// <summary>
        public ReshapeItemDescription() { this.TypeDescriminator = this.GetType().Name; }

        /// <summary>
        /// Type descriminator for json serialization
        /// <summary>
        [JsonProperty("$type")]
        public string TypeDescriminator { get; set; }

        /// <summary>
        /// Name of new variable created by this command.
        /// <summary>
        public string TargetVariableName { get; set; }
        /// <summary>
        /// Label for new variable created by this command.
        /// <summary>
        public string TargetVariableLabel { get; set; }
        /// <summary>
        /// Source variables in the original dataset used to create this variable.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public VariableReferenceBase SourceVariables { get; set; }
        /// <summary>
        /// The stub is a string used in the names of variables in the wide dataset.
        /// <summary>
        public string Stub { get; set; }
        /// <summary>
        /// A list of values that produce new rows (long) or columns (wide) for this variable.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public ExpressionBase IndexValues { get; set; }
        /// <summary>
        /// Name of the variable that will contain the value of the index for this row.
        /// <summary>
        public string IndexVariableName { get; set; }
        /// <summary>
        /// Label for the variable in IndexVariableName.
        /// <summary>
        public string IndexVariableLabel { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public virtual XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            if (TargetVariableName != null)
            {
                xEl.Add(new XElement(ns + "TargetVariableName", TargetVariableName));
            }
            if (TargetVariableLabel != null)
            {
                xEl.Add(new XElement(ns + "TargetVariableLabel", TargetVariableLabel));
            }
            if (SourceVariables != null) { xEl.Add(SourceVariables.ToXml("SourceVariables")); }
            if (Stub != null)
            {
                xEl.Add(new XElement(ns + "Stub", Stub));
            }
            if (IndexValues != null) { xEl.Add(IndexValues.ToXml("IndexValues")); }
            if (IndexVariableName != null)
            {
                xEl.Add(new XElement(ns + "IndexVariableName", IndexVariableName));
            }
            if (IndexVariableLabel != null)
            {
                xEl.Add(new XElement(ns + "IndexVariableLabel", IndexVariableLabel));
            }
            return xEl;
        }
    }
}

