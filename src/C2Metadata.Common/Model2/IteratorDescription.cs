using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Describes an iteration process consisting of an IteratorSymbolExpression and a list of values it takes.  
    /// 
    /// <summary>
    public partial class IteratorDescription
    {

        /// <summary>
        /// Set the TypeDescriminator
        /// <summary>
        public IteratorDescription() { this.TypeDescriminator = this.GetType().Name; }

        /// <summary>
        /// Type descriminator for json serialization
        /// <summary>
        [JsonProperty("$type")]
        public string TypeDescriminator { get; set; }

        /// <summary>
        /// The name used in  IteratorCommands  for the index variable that changes value in the loop.
        /// <summary>
        public IteratorSymbolExpression IteratorSymbolName { get; set; }
        /// <summary>
        /// Describes the values that are substituted for the index variable that changes value in the loop.
        /// <summary>
        [JsonConverter(typeof(SubstitutionConverter))]
        public List<ExpressionBase> IteratorValues { get; set; } = new List<ExpressionBase>();
        public bool ShouldSerializeIteratorValues() { return IteratorValues.Count > 0; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public virtual XElement ToXml(string name)
        {
            XNamespace ns = "";
            XElement xEl = new XElement(ns + name);
            if (IteratorSymbolName != null) { xEl.Add(IteratorSymbolName.ToXml("IteratorSymbolName")); }
            if (IteratorValues != null && IteratorValues.Count > 0)
            {
                foreach (var item in IteratorValues)
                {
                    xEl.Add(item.ToXml("IteratorValues"));
                }
            }
            return xEl;
        }
    }
}

