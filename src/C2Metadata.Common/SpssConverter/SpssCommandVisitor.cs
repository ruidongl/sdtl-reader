﻿using C2Metadata.SpssToSdtl.Grammar;
using System;
using System.Collections.Generic;
using System.Text;
using Antlr4.Runtime.Misc;
using Antlr4.Runtime.Tree;
using System.Linq;
using sdtl;
using C2Metadata.Common.Model;

namespace C2Metadata.Common.SpssConverter
{
    public class SpssCommandVisitor : SpssBaseVisitor<TransformBase>
    {
        public string SourceFileName { get; internal set; }

        public List<Message> Messages { get; } = new List<Message>();

        public override TransformBase VisitProgram([NotNull] SpssParser.ProgramContext context)
        {
            return base.VisitProgram(context);
        }

        public override TransformBase VisitChildren(IRuleNode node)
        {
            var commandList = new CommandList();

            if (node == null)
            {
                return commandList;
            }

            int n = node.ChildCount;
            for (int i = 0; i < n; i++)
            {
                if (!ShouldVisitNextChild(node, commandList))
                {
                    break;
                }

                var c = node.GetChild(i);
                var childResult = c.Accept(this);
                AggregateResult(commandList, childResult);
            }

            return commandList;
        }

        protected override TransformBase AggregateResult(TransformBase aggregate, TransformBase nextResult)
        {
            if (nextResult == null)
            {
                return aggregate;
            }

            var mainList = aggregate as CommandList;
            if (mainList != null)
            {
                var newList = nextResult as CommandList;
                if (newList != null)
                {
                    foreach (var child in newList.Commands)
                    {
                        if (child != null)
                        {
                            mainList.Commands.Add(child);
                        }
                    }
                }
                else
                {
                    mainList.Commands.Add(nextResult);
                }
            }

            return mainList;
        }

        public override TransformBase VisitTitle_command([NotNull] SpssParser.Title_commandContext context)
        {
            string title = context.title?.Text.Trim(new char[] { '"' });
            if (context.title?.StartIndex < 0)
            {
                var invalid = new Invalid
                {
                    Message = "TITLE command is missing the title"
                };

                return invalid;
            }

            var result = new SetDatasetProperty();
            result.PropertyName = "Title";
            result.Value = title;

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitSubtitle_command([NotNull] SpssParser.Subtitle_commandContext context)
        {
            string subtitle = context.subtitle?.Text.Trim(new char[] { '"' });
            if (context.subtitle?.StartIndex < 0)
            {
                var invalid = new Invalid
                {
                    Message = "SUBTITLE command is missing the subtitle"
                };

                return invalid;
            }

            var result = new SetDatasetProperty();
            result.PropertyName = "Subtitle";
            result.Value = subtitle;

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitSave_command([NotNull] SpssParser.Save_commandContext context)
        {
            if (context.outfile == null)
            {
                var invalid = new Invalid
                {
                    Message = "SAVE command is missing OUTFILE"
                };

                return invalid;
            }

            if (context.compressed != null &&
                context.uncompressed != null)
            {
                var invalid = new Invalid
                {
                    Message = "SAVE command cannot specify both COMPRESSED and UNCOMPRESSED"
                };

                return invalid;
            }

            var saveCommand = new Save();
            saveCommand.FileName = context.outfile.Text.Trim(new char[] { '"' });
           saveCommand.IsCompressed = context.compressed != null;

            if (context.keepVarList != null)
            {
                // Create a separate KeepVariables command, if the /keep subcommand is specified.
                var keepCommand = new KeepVariables();
                keepCommand.Variables = GetVariablesFromListWithRange(context.keepVarList);
                RecordSourceInformation(keepCommand, context);

                var list = new CommandList();
                list.Commands.Add(keepCommand);
                list.Commands.Add(saveCommand);
                return list;
            }

            RecordSourceInformation(saveCommand, context);
            return saveCommand;
        }

        public override TransformBase VisitRecode_command([NotNull] SpssParser.Recode_commandContext context)
        {
            if (context.varlist == null)
            {
                var invalid = new Invalid
                {
                    Message = "RECODE command is missing varlist"
                };

                return invalid;
            }

            var result = new Recode();

            string[] varlistIds = context.varlist.IDENTIFIER()
                .Select(x => x.GetText())
                .ToArray();

            string[] intoIds = context.intovars?.IDENTIFIER()
                .Select(x => x.GetText())
                .ToArray();

            if (intoIds != null &&
                varlistIds.Length != intoIds.Length)
            {
                var invalid = new Invalid
                {
                    Message = "RECODE varlist must be the same length as the INTO list."
                };

                return invalid;
            }

            // varlist and matching with INTO variables
            for (int i = 0; i < context.varlist.ChildCount; i++)
            {
                var varlistItem = context.varlist.IDENTIFIER(i);

                string text = varlistItem.GetText();
                var recodeVariable = new RecodeVariable();
                recodeVariable.Source = varlistItem.GetText();

                if (intoIds != null)
                {
                    recodeVariable.Target = intoIds[i];
                }
                else
                {
                    recodeVariable.Target = recodeVariable.Source;
                }

                result.RecodedVariables.Add(recodeVariable);
            }

            // Rules
            for (int ruleIdx = 0; ruleIdx < context.numeric_value_list().Length; ruleIdx++)
            {
                var rule = new RecodeRule();

                // From
                var valueList = context.numeric_value_list(ruleIdx).numeric_value_list_member();
                foreach (var valueNode in valueList)
                {
                    if (valueNode.numeric_input_value()?.ELSE() != null)
                    {
                        rule.FromValue.Add(new UnhandledValuesExpression());
                    }
                    else if (valueNode.numeric_input_value() != null)
                    {
                        string numericValue = valueNode.numeric_input_value().GetText();
                        var numExpr = new NumericConstantExpression() { Value = numericValue };
                        rule.FromValue.Add(numExpr);
                    }
                    else if (valueNode.thru() != null)
                    {
                        string low = valueNode.thru().lowValue.GetText();
                        string high = valueNode.thru().highValue.GetText();
                        var range = new NumberRangeExpression();

                        if (low.ToUpper().StartsWith("LO"))
                        {
                            range.NumberRangeStart = new NumericMinimumValueExpression();
                        }
                        else
                        {
                            range.NumberRangeStart = new NumericConstantExpression() { Value = low };
                        }

                        if (high.ToUpper().StartsWith("HI"))
                        {
                            range.NumberRangeEnd = new NumericMaximumValueExpression();
                        }
                        else
                        {
                            range.NumberRangeEnd = new NumericConstantExpression() { Value = high };
                        }

                        rule.FromValue.Add(range);
                    }
                }

                // To
                var numericOutput = context.numeric_output(ruleIdx);
                if (numericOutput?.COPY() != null)
                {
                    rule.To = "copy";
                }
                else
                {
                    string outputStr = numericOutput?.GetText();
                    rule.To = outputStr;
                }

                result.Rules.Add(rule);
            }

            for (int ruleIdx = 0; ruleIdx < context.string_value_list().Length; ruleIdx++)
            {
                var rule = new RecodeRule();

                // From
                var valueList = context.string_value_list(ruleIdx).string_input_value();
                foreach (var valueNode in valueList)
                {
                    if (valueNode?.ELSE() != null)
                    {
                        rule.FromValue.Add(new UnhandledValuesExpression());
                    }
                    else if (valueNode != null)
                    {
                        string str = valueNode.GetText();
                        var strExpr = new StringConstantExpression { Value = str };
                        rule.FromValue.Add(strExpr);
                    }
                }

                // To (string)
                var stringOutput = context.string_output(ruleIdx);
                if (stringOutput?.COPY() != null)
                {
                    rule.To = "copy";
                }
                else if (stringOutput != null)
                {
                    string outputStr = stringOutput.GetText();
                    rule.To = outputStr;
                }

                // To (numeric)
                var numericOutput = context.numeric_output(ruleIdx);
                if (numericOutput.COPY() != null)
                {
                    rule.To = "copy";
                }
                else if (numericOutput != null)
                {
                    string outputStr = numericOutput.GetText();
                    rule.To = outputStr;
                }

                result.Rules.Add(rule);
            }


            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitRename_command([NotNull] SpssParser.Rename_commandContext context)
        {
            if (context.pairs == null)
            {
                var invalid = new Invalid
                {
                    Message = "RENAME command is missing varlist"
                };

                return invalid;
            }

            var result = new Rename();

            foreach (var pairContext in context.children.OfType<SpssParser.Rename_pairContext>())
            {
                var pair = new RenamePair();
                result.Renames.Add(pair);

                if (pairContext.oldname != null)
                {
                    pair.OldVariable = new VariableSymbolExpression { VariableName = pairContext.oldname.Text };
                }
                else
                {
                    Messages.Add(new Message
                    {
                        Severity = "Warning",
                        MessageText = "RENAME: cannot detect old name"
                    });
                }

                if (pairContext.newname != null)
                {
                    pair.NewVariable = new VariableSymbolExpression { VariableName = pairContext.newname.Text };
                }
                else
                {
                    Messages.Add(new Message
                    {
                        Severity = "Warning",
                        MessageText = "RENAME: cannot detect new name"
                    });
                }

            }

            RecordSourceInformation(result, context);
            return result;
        }

        //public override TransformBase VisitExecute_command([NotNull] SpssParser.Execute_commandContext context)
        //{
        //    var result = new Execute();
        //    RecordSourceInformation(result, context);
        //    return result;
        //}

        public override TransformBase VisitGet_command([NotNull] SpssParser.Get_commandContext context)
        {
            if (context.filename == null)
            {
                var invalid = new Invalid
                {
                    Message = "GET command is missing FILE"
                };

                return invalid;
            }

            var result = new Load();
            result.FileName = context.filename.Text.Trim(new char[] { '"' });

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitCompute_command([NotNull] SpssParser.Compute_commandContext context)
        {
            // TODO Check for validity.

            var result = new Compute();

            result.Variable = new VariableSymbolExpression
            {
                VariableName = context.variable.Text
            };
            result.Expression = ParseExpression(context.expression);

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitVariable_labels_command([NotNull] SpssParser.Variable_labels_commandContext context)
        {
            var result = new SetVariableLabel();

            result.Variable = new VariableSymbolExpression
            {
                VariableName = context.variable?.Text
            };

            if (!string.IsNullOrWhiteSpace(context.label?.Text))
            {
                result.Label = StripQuotes(context.label.Text);
            }

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitValue_labels_command([NotNull] SpssParser.Value_labels_commandContext context)
        {
            var result = new SetValueLabels();

            var variables = GetVariablesFromListWithRange(context.var_list_with_range());
            result.Variables.AddRange(variables);

            if (variables.Count == 0)
            {
                AddMessage(context.Start.Line, context.Start.Column, "Warning", "Set Value Label: no variable name found");
            }

            int pairCount = context.NUMERIC_LITERAL().Length;
            for (int i = 0; i < pairCount; i++)
            {
                string value = context.NUMERIC_LITERAL()[i].GetText();

                string label = string.Empty;
                if (context.STRING().Length > i)
                {
                    label = StripQuotes(context.STRING()[i]?.GetText());
                }

                result.Labels.Add(new ValueLabel
                {
                    Value = value,
                    Label = label
                });
            }

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitPrint_format_command([NotNull] SpssParser.Print_format_commandContext context)
        {
            var result = new SetDisplayFormat();

            var variables = GetVariablesFromListWithRange(context.var_list_with_range());
            result.Variables.AddRange(variables);

            if (context.formatName != null)
            {
                result.Format = context.formatName.Text;
            }
            else
            {
                AddMessage(context.Start.Line, context.Start.Column, "Warning", "Set Display Format: no format name");
            }

            RecordSourceInformation(result, context);
            return result;
        }

        private List<VariableReferenceBase> GetVariablesFromListWithRange(SpssParser.Var_list_with_rangeContext var_list_with_range)
        {
            var variables = new List<VariableReferenceBase>();

            if (var_list_with_range == null)
            {
                return variables;
            }

            foreach (var x in var_list_with_range.IDENTIFIER())
            {
                variables.Add(new VariableSymbolExpression { VariableName = x.GetText() });
            }
            foreach (var x in var_list_with_range.var_range())
            {
                var range = new VariableRangeExpression();
                range.First = x.startVariable.Text;
                range.Last = x.endVariable.Text;

                variables.Add(range);
            }

            return variables;
        }

        public override TransformBase VisitMissing_values_command([NotNull] SpssParser.Missing_values_commandContext context)
        {
            var result = new CommandList();

            var count = context.var_list_with_range().Length;
            for (int i = 0; i < count; i++)
            {
                var command = new SetMissingValues();
                result.Commands.Add(command);

                if (context.var_list_with_range()[i].var_range()?.Count() > 0)
                {
                    var range = context.var_list_with_range()[i].var_range()[0];

                    var variableRange = new VariableRangeExpression();
                    variableRange.First = range.startVariable.Text;
                    variableRange.Last = range.endVariable.Text;

                    command.Variables.Add(variableRange);
                }
                else
                {
                    var variables = context.var_list_with_range()[i].IDENTIFIER()
                        .Select(x => new VariableSymbolExpression { VariableName = x.GetText() })
                        .ToArray();
                    command.Variables.AddRange(variables);
                }

                if (context.numeric_value_list().Length > i)
                {
                    var valueList = context.numeric_value_list()[i].numeric_value_list_member();
                    foreach (var valueNode in valueList)
                    {
                        if (valueNode.numeric_input_value() != null)
                        {
                            string numValue = valueNode.numeric_input_value().GetText();
                            var numExpr = new NumericConstantExpression { Value = numValue };
                            command.Values.Add(numExpr);
                        }
                        else if (valueNode.thru() != null)
                        {
                            string low = valueNode.thru().lowValue.GetText();
                            string high = valueNode.thru().highValue.GetText();

                            var range = new NumberRangeExpression();
                            if (low.ToUpper().StartsWith("LO"))
                            {
                                range.NumberRangeStart = new NumericMinimumValueExpression();
                            }
                            else
                            {
                                range.NumberRangeStart = new NumericConstantExpression() { Value = low };
                            }

                            if (high.ToUpper().StartsWith("HI"))
                            {
                                range.NumberRangeEnd = new NumericMaximumValueExpression();
                            }
                            else
                            {
                                range.NumberRangeEnd = new NumericConstantExpression() { Value = high };
                            }

                            command.Values.Add(range);
                        }
                    }
                }

                RecordSourceInformation(command, context);

            }


            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitAdd_files_command([NotNull] SpssParser.Add_files_commandContext context)
        {
            var result = new AppendDatasets();

            foreach (ITerminalNode fileNameNode in context.STRING())
            {
                string fileName = StripQuotes(fileNameNode.GetText());
                result.FileName.Add(fileName);
            }

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitMatch_files_command([NotNull] SpssParser.Match_files_commandContext context)
        {
            var result = new MergeDatasets();

            foreach (var fileDeclaration in context.match_files_file_declaration())
            {
                string fileName = StripQuotes(fileDeclaration.filename?.Text);

                var descr = new MergeFileDescription()
                {
                    FileName = fileName,
                    MergeType = "OneToOne"
                };

                result.MergeFiles.Add(descr);
            }

            result.MergeByVariables = new VariableSymbolExpression { VariableName = context.by.Text };

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitAggregate_command([NotNull] SpssParser.Aggregate_commandContext context)
        {
            string datasetName = context.datasetName?.Text;
            bool isInPlace = string.IsNullOrWhiteSpace(datasetName) || datasetName == "*";

            TransformBase result = null;
            if (isInPlace)
            {
                var aggregate = new Aggregate();
                result = aggregate;

                // BREAK (which variables to group by)
                foreach (var breakIdentifier in context.var_list().IDENTIFIER())
                {
                    aggregate.GroupByVariables.Add(new VariableSymbolExpression { VariableName = breakIdentifier.GetText() });
                }

                foreach (var aggVarDef in context.aggregateVariableDefinition())
                {
                    var compute = new Compute();
                    compute.Variable = new VariableSymbolExpression { VariableName = aggVarDef.aggregateVariableName.Text };
                    compute.Expression = ParseFunctionCall(aggVarDef.function_call());
                    aggregate.AggregateVariables.Add(compute);
                }

            }
            else
            {
                var collapse = new Collapse();
                result = collapse;

                // Dataset name
                collapse.OutputDatasetName = datasetName;

                // BREAK (which variables to group by)
               if (context.var_list() != null)
                {
                    foreach (var breakIdentifier in context.var_list().IDENTIFIER())
                    {
                        collapse.GroupByVariables.Add(new VariableSymbolExpression { VariableName = breakIdentifier.GetText() });
                    }
                }

               if (context.aggregateVariableDefinition() != null)
                {
                    foreach (var aggDef in context.aggregateVariableDefinition())
                    {
                        string name = aggDef.aggregateVariableName.Text;
                        var functionCall = ParseFunctionCall(aggDef.function_call());

                        var compute = new Compute();
                        compute.Variable = new VariableSymbolExpression { VariableName = name };
                        compute.Expression = functionCall;
                        collapse.AggregateVariables.Add(compute);
                    }
                }
            }

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitDo_repeat_command([NotNull] SpssParser.Do_repeat_commandContext context)
        {
            var result = new LoopOverList();

            // Which variables does this loop iterate over?
            //var variables = GetVariablesFromListWithRange(context.var_list_with_range());

            var iterator = new IteratorDescription();
            //iterator.IteratorValues.AddRange(variables);
            iterator.IteratorSymbolName = new IteratorSymbolExpression { Name = context.iterator.Text };
            result.Iterators.Add(iterator);

            // Set the local iterator variable.

            // For every command within the loop, create a transform and add it to the commands list.
            var commands = context.command();
            if (commands != null)
            {
                foreach (var command in commands)
                {
                    var commandList = VisitCommand(command) as CommandList;
                    foreach (var x in commandList.Commands)
                    {
                        result.Commands.Add(x);
                    }
                }
            }

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitLoop_command([NotNull] SpssParser.Loop_commandContext context)
        {
            var result = new LoopOverList();

            // Start and End expressions
            var iterator = new IteratorDescription();

            var range = new StringRangeExpression();
            //range.RangeStart = ParseExpression(context.startExpression);
            //range.RangeEnd = ParseExpression(context.endExpression);
            iterator.IteratorValues.Add(range);

            iterator.IteratorSymbolName = new IteratorSymbolExpression { Name = context.iterator.Text };
            result.Iterators.Add(iterator);

            // For every command within the loop, create a transform and add it to the commands list.
            var commands = context.command();
            if (commands != null)
            {
                foreach (var command in commands)
                {
                    var commandList = VisitCommand(command) as CommandList;
                    foreach (var x in commandList.Commands)
                    {
                        result.Commands.Add(x);
                    }
                }
            }

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitIf_command([NotNull] SpssParser.If_commandContext context)
        {
            var result = new Compute();

            result.Condition = ParseCondition(context.condition());
            result.Variable = new VariableSymbolExpression
            {
                VariableName = context.variable?.Text
            };

            if (context.expression != null)
            {
                result.Expression = ParseExpression(context.expression);
            }

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitSelect_if_command([NotNull] SpssParser.Select_if_commandContext context)
        {
            var result = new Select();

            result.Condition = ParseCondition(context.condition());

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitDelete_variables_command([NotNull] SpssParser.Delete_variables_commandContext context)
        {
            var result = new DeleteVariables();
            result.Command = "delete";

            var variables = GetVariablesFromListWithRange(context.var_list_with_range());
            result.Variables.AddRange(variables);

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitComment_command([NotNull] SpssParser.Comment_commandContext context)
        {
            var result = new Comment();
            result.Command = "comment";

            string text = context.GetText().Trim();
            if (text.StartsWith("* "))
            {
                text = text.Substring(2).Trim();
            }
            else if (text.ToLower().StartsWith("comment "))
            {
                text = text.Substring(8).Trim();
            }
            result.CommentText = text;

            RecordSourceInformation(result, context);
            return result;
        }

        private ExpressionBase ParseExpression(SpssParser.Transformation_expressionContext expression)
        {
            if (expression.OPEN_PAREN() != null &&
                expression.CLOSE_PAREN() != null)
            {
                var result = new GroupedExpression();

                if (expression.transformation_expression().Length > 0)
                {
                    var childExpression = expression.transformation_expression()[0];
                    var childParsed = ParseExpression(childExpression);
                    result.Expression = childParsed;
                }
                else
                {
                    Messages.Add(new Message
                    {
                        Severity = "Warning",
                        MessageText = $"Grouped expression: cannot detect expressions"
                    });
                }

                return result;
            }

            else if (expression.PLUS() != null)
            {
                var result = new FunctionCallExpression();
                result.Function = "addition";
                result.IsSdtlName = true;
                HandleConditionsForFunctionCallTransformation(expression.transformation_expression(), result, "PLUS");
                return result;
            }

            else if (expression.MINUS() != null)
            {
                var result = new FunctionCallExpression();
                result.Function = "subtraction";
                result.IsSdtlName = true;
                HandleConditionsForFunctionCallTransformation(expression.transformation_expression(), result, "MINUS");
                return result;
            }

            else if (expression.STAR() != null)
            {
                var result = new FunctionCallExpression();
                result.Function = "multiplication";
                result.IsSdtlName = true;
                HandleConditionsForFunctionCallTransformation(expression.transformation_expression(), result, "STAR");
                return result;
            }

            else if (expression.SLASH() != null)
            {
                var result = new FunctionCallExpression();
                result.Function = "division";
                result.IsSdtlName = true;
                HandleConditionsForFunctionCallTransformation(expression.transformation_expression(), result, "SLASH");
                return result;
            }

            else if (expression.STARSTAR() != null)
            {
                var result = new FunctionCallExpression();
                result.Function = "exponentiation";
                result.IsSdtlName = true;
                HandleConditionsForFunctionCallTransformation(expression.transformation_expression(), result, "STARSTAR");
                return result;
            }

            else if (expression.function_call() != null)
            {
                var result = ParseFunctionCall(expression.function_call());
                return result;
            }

            else if (expression.IDENTIFIER() != null)
            {
                var result = new VariableSymbolExpression();
                result.VariableName = expression.IDENTIFIER().GetText();
                return result;
            }

            else if (expression.NUMERIC_LITERAL() != null)
            {
                string str = expression.NUMERIC_LITERAL().GetText();
                var result = new NumericConstantExpression();
                result.Value = str;

                // Figure out the numeric type.
                if (int.TryParse(str, out var number))
                {
                    result.NumericType = "int";
                }
                else if (double.TryParse(str, out var dNumber))
                {
                    result.NumericType = "double";
                }

                return result;
            }

            else if (expression.STRING() != null)
            {
                var result = new StringConstantExpression();
                string str = expression.STRING().GetText();

                // Remove first and last character (the quotes).
                str = StripQuotes(str);
                result.Value = str;
                return result;
            }

            return null;
        }

        private FunctionCallExpression ParseFunctionCall(SpssParser.Function_callContext function_call)
        {
            string functionName = function_call.IDENTIFIER().GetText();

            var expressionList = new List<ExpressionBase>();
            var parameterList = function_call.parameter_list();
            foreach (var param in parameterList.parameter())
            {
                var expr = param.transformation_expression();
                if (expr != null)
                {
                    var exprParam = ParseExpression(expr);
                    expressionList.Add(exprParam);
                }

                if (param.var_list_with_range() != null)
                {
                    var variableList = new VariableListExpression();
                    expressionList.Add(variableList);

                    var variables = GetVariablesFromListWithRange(param.var_list_with_range());
                    variableList.Variables.AddRange(variables);
                }

            }

            var mapper = new SpssFunctionMapper();
            var result = mapper.CreateFunctionCallExpression(functionName, expressionList);

            return result;
        }

        private ExpressionBase ParseCondition(SpssParser.ConditionContext condition)
        {
            if (condition.AND() != null ||
                condition.AMPERSAND() != null)
            {
                var result = new FunctionCallExpression();
                result.Function = "and";
                result.IsSdtlName = true;
                HandleConditionsForFunctionCallCondition(condition, result, "AND");
                return result;
            }

            else if (condition.OR() != null ||
                condition.PIPE() != null)
            {
                var result = new FunctionCallExpression();
                result.Function = "or";
                result.IsSdtlName = true;
                HandleConditionsForFunctionCallCondition(condition, result, "OR");
                return result;
            }

            else if (condition.NOT() != null)
            {
                var result = new FunctionCallExpression();
                result.Function = "not";
                result.IsSdtlName = true;

                if (condition.condition().Length > 0)
                {
                    var arg = new FunctionArgument();
                    arg.ArgumentValue = ParseCondition(condition.condition()[0]);
                    result.Arguments.Add(arg);
                }
                else
                {
                    Messages.Add(new Message
                    {
                        Severity = "Warning",
                        MessageText = "NOT expression: cannot detect expression"
                    });
                }

                return result;
            }

            else if (condition.OPEN_PAREN() != null &&
                condition.CLOSE_PAREN() != null)
            {
                if (condition.condition().Length > 0)
                {
                    var childCondition = condition.condition()[0];
                    return ParseCondition(childCondition);
                }
                else
                {
                    Messages.Add(new Message
                    {
                        Severity = "Warning",
                        MessageText = "Grouped expression: cannot detect expression"
                    });
                }
            }

            else if (condition.EQUAL() != null ||
                condition.EQ() != null)
            {
                if (condition.transformation_expression().Length < 2) return null;

                var result = new FunctionCallExpression();
                result.Function = "eq";
                result.IsSdtlName = true;
                HandleConditionsForFunctionCallTransformation(condition.transformation_expression(), result, "EQUAL");
                return result;
            }

            else if (condition.NOTEQUAL() != null ||
                condition.NOTEQUAL2() != null)
            {
                var result = new FunctionCallExpression();
                result.Function = "ne";
                result.IsSdtlName = true;
                HandleConditionsForFunctionCallTransformation(condition.transformation_expression(), result, "NOTEQUAL");
                return result;
            }

            else if (condition.GT() != null ||
                condition.GT2() != null)
            {
                var result = new FunctionCallExpression();
                result.Function = "gt";
                result.IsSdtlName = true;
                HandleConditionsForFunctionCallTransformation(condition.transformation_expression(), result, "GreaterThan");
                return result;
            }

            else if (condition.GT_EQ() != null ||
                condition.GE() != null)
            {
                var result = new FunctionCallExpression();
                result.Function = "ge";
                result.IsSdtlName = true;
                HandleConditionsForFunctionCallTransformation(condition.transformation_expression(), result, "GreaterThanOrEqual");
                return result;
            }

            else if (condition.LT() != null ||
                condition.LT2() != null)
            {
                var result = new FunctionCallExpression();
                result.Function = "lt";
                result.IsSdtlName = true;
                HandleConditionsForFunctionCallTransformation(condition.transformation_expression(), result, "LessThan");
                return result;
            }

            else if (condition.LT_EQ() != null ||
                condition.LE() != null)
            {
                var result = new FunctionCallExpression();
                result.Function = "le";
                result.IsSdtlName = true;
                HandleConditionsForFunctionCallTransformation(condition.transformation_expression(), result, "LessThanOrEqual");
                return result;
            }

            return null;
        }

        private void HandleConditionsForFunctionCallCondition(SpssParser.ConditionContext condition, FunctionCallExpression expr, string expressionType)
        {
            var conditionsTree = condition.condition();
            if (conditionsTree.Length > 0)
            {
                var arg = new FunctionArgument();
                arg.ArgumentValue = ParseCondition(conditionsTree[0]);
                expr.Arguments.Add(arg);
            }
            if (conditionsTree.Length > 1)
            {
                var arg = new FunctionArgument();
                arg.ArgumentValue = ParseCondition(conditionsTree[1]);
                expr.Arguments.Add(arg);
            }

            if (conditionsTree.Length < 2)
            {
                Messages.Add(new Message
                {
                    Severity = "Warning",
                    MessageText = $"{expressionType} expression: cannot detect expressions"
                });
            }
        }

        private void HandleConditionsForFunctionCallTransformation(SpssParser.Transformation_expressionContext[] expressionTree, FunctionCallExpression expr, string expressionType)
        {
            if (expressionTree.Length > 0)
            {
                var arg = new FunctionArgument();
                arg.ArgumentValue = ParseExpression(expressionTree[0]);
                expr.Arguments.Add(arg);
            }
            if (expressionTree.Length > 1)
            {
                var arg = new FunctionArgument();
                arg.ArgumentValue = ParseExpression(expressionTree[1]);
                expr.Arguments.Add(arg);
            }

            if (expressionTree.Length < 2)
            {
                Messages.Add(new Message
                {
                    Severity = "Warning",
                    MessageText = $"{expressionType} expression: cannot detect expressions"
                });
            }
        }

        private static string StripQuotes(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return str;
            }

            str = str.Substring(1, str.Length - 2);
            return str;
        }

        private void RecordSourceInformation(TransformBase command, Antlr4.Runtime.ParserRuleContext context)
        {
            command.SourceInformation = new SourceInformation();
            command.SourceInformation.SourceStartIndex = context.Start.StartIndex;
            command.SourceInformation.SourceStopIndex = context.Stop.StopIndex;
            command.SourceInformation.LineNumberStart = context.Start.Line - 1;
            command.SourceInformation.LineNumberEnd = context.Stop.Line - 1;
            command.SourceInformation.OriginalSourceText = context.Start.InputStream
                .ToString()
                .Substring(context.Start.StartIndex, context.Stop.StopIndex - context.Start.StartIndex + 1)
                .Trim();
        }

        private void AddMessage(int lineNumber, int column, string severity, string text)
        {
            Messages.Add(new Message
            {
                LineNumber = lineNumber,
                CharacterPosition = column,
                Severity = severity,
                MessageText = text
            });
        }


        #region Unsupported commands

        public override TransformBase VisitFrequencies_command([NotNull] SpssParser.Frequencies_commandContext context)
        {
            return HandleUnsupportedCommand(context, true);
        }

        public override TransformBase VisitAlter_command([NotNull] SpssParser.Alter_commandContext context)
        {
            return HandleUnsupportedCommand(context, false);
        }

        public override TransformBase VisitCases_to_vars_command([NotNull] SpssParser.Cases_to_vars_commandContext context)
        {
            return HandleUnsupportedCommand(context, false);
        }

        public override TransformBase VisitCount_command([NotNull] SpssParser.Count_commandContext context)
        {
            return HandleUnsupportedCommand(context, true);
        }

        public override TransformBase VisitList_variables_command([NotNull] SpssParser.List_variables_commandContext context)
        {
            return HandleUnsupportedCommand(context, true);
        }

        public override TransformBase VisitTemporary_command([NotNull] SpssParser.Temporary_commandContext context)
        {
            return HandleUnsupportedCommand(context, false);
        }

        public override TransformBase VisitData_list_command([NotNull] SpssParser.Data_list_commandContext context)
        {
            return HandleUnsupportedCommand(context, false);
        }

        public override TransformBase VisitBegin_data_command([NotNull] SpssParser.Begin_data_commandContext context)
        {
            return HandleUnsupportedCommand(context, false);
        }

        public override TransformBase VisitNew_file_command([NotNull] SpssParser.New_file_commandContext context)
        {
            return HandleUnsupportedCommand(context, false);
        }

        public override TransformBase VisitInput_program_command([NotNull] SpssParser.Input_program_commandContext context)
        {
            return HandleUnsupportedCommand(context, false);
        }

        public override TransformBase VisitSort_variables_command([NotNull] SpssParser.Sort_variables_commandContext context)
        {
            return HandleUnsupportedCommand(context, false);
        }

        public override TransformBase VisitMeans_command([NotNull] SpssParser.Means_commandContext context)
        {
            return HandleUnsupportedCommand(context, true);
        }

        public override TransformBase VisitCrosstabs_command([NotNull] SpssParser.Crosstabs_commandContext context)
        {
            return HandleUnsupportedCommand(context, true);
        }

        private TransformBase HandleUnsupportedCommand(Antlr4.Runtime.ParserRuleContext context, bool isAnalysis)
        {
            if (isAnalysis)
            {
                var result = new Analysis();
                RecordSourceInformation(result, context);
                return result;
            }
            else
            {
                var result = new Unsupported();
                result.Message = "This command is not yet supported.";
                RecordSourceInformation(result, context);
                return result;
            }
        }

        #endregion

    }
}
