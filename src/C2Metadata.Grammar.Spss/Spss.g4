grammar Spss;

// Parser Rules

program
    : (NEWLINE? command)* EOF
    ;

command:   
      alter_command
    | recode_command
    | rename_command
    | title_command
    | subtitle_command
    | save_command
    | execute_command
    | get_command
    | compute_command
    | variable_labels_command
    | value_labels_command
    | print_format_command
    | missing_values_command
    | add_files_command
    | match_files_command
    | do_repeat_command
    | do_if_command
    | if_command
    | select_if_command
    | delete_variables_command
    | sort_cases_command
    | cases_to_vars_command
    | aggregate_command
    | frequencies_command
    | count_command
    | list_variables_command
    | temporary_command
    | empty_command
    | comment_command
    | data_list_command
    | begin_data_command
    | new_file_command
    | input_program_command
    | sort_variables_command
    | means_command
    | loop_command
    | crosstabs_command
    | cd_command
    | define_command
    | function_call_command
    | set_mprint_command
    ;

title_command: TITLE title=STRING END_COMMAND;
subtitle_command: SUBTITLE subtitle=STRING END_COMMAND;
execute_command: EXECUTE END_COMMAND;

save_command: SAVE 
    OUTFILE EQUAL outfile=STRING 
    ( SLASH compressed=COMPRESSED |
      SLASH uncompressed=UNCOMPRESSED |
      SLASH KEEP EQUAL? keepVarList=var_list_with_range
    )* 
    END_COMMAND;

get_command: GET FILE EQUAL filename=STRING END_COMMAND;

recode_command: 
    RECODE varlist=var_list (OPEN_PAREN inputs=numeric_value_list EQUAL output=numeric_output CLOSE_PAREN)+ (INTO intovars=var_list)?
    (SLASH var_list (OPEN_PAREN numeric_value_list EQUAL numeric_output CLOSE_PAREN)+ (INTO var_list)? )?
    END_COMMAND

    | RECODE varlist=var_list (OPEN_PAREN string_value_list EQUAL string_output CLOSE_PAREN)+ (INTO intovars=var_list)?
    (SLASH var_list (OPEN_PAREN string_value_list EQUAL string_output CLOSE_PAREN)+ (INTO var_list)?)?
    END_COMMAND

    | RECODE varlist=var_list (OPEN_PAREN string_value_list EQUAL output=numeric_output CLOSE_PAREN)+ INTO intovars=var_list?
    (SLASH var_list (OPEN_PAREN string_value_list EQUAL output=numeric_output CLOSE_PAREN)+ INTO var_list?)?
    END_COMMAND
    ;

compute_command:
    COMPUTE variable=IDENTIFIER EQUAL expression=transformation_expression END_COMMAND;

variable_labels_command:
    variable_labels_command_header
    (variable=IDENTIFIER label=STRING)+ 
    END_COMMAND;

variable_labels_command_header:
      VARIABLE LABELS
    | VAR LABEL;

value_labels_command:
    ADD? VALUE LABELS varlist=var_list_with_range (value=NUMERIC_LITERAL label=STRING)+ END_COMMAND;

print_format_command:
    PRINT? FORMATS varlist=var_list_with_range OPEN_PAREN (formatName=.*) CLOSE_PAREN END_COMMAND;

missing_values_command:
    (MISSING VALUES | MISSING VALUE)
    (varlist=var_list_with_range OPEN_PAREN numeric_value_list? CLOSE_PAREN)+ END_COMMAND;

add_files_command:
    ADD FILES (SLASH? FILE EQUAL fileName=STRING)+ END_COMMAND;

match_files_command: 
    MATCH FILES 
    match_files_file_declaration+
    SLASH BY by=IDENTIFIER
    END_COMMAND;

match_files_file_declaration:
    SLASH FILE EQUAL (filename=STRING | STAR) (SLASH IN EQUAL IDENTIFIER)?;    

do_repeat_command:
    DO REPEAT iterator=IDENTIFIER EQUAL var_list_with_range 
    (SLASH IDENTIFIER EQUAL var_list_with_range)*
    END_COMMAND
    commands=command*
    END REPEAT END_COMMAND;

loop_command: 
    LOOP iterator=IDENTIFIER EQUAL startExpression=transformation_expression TO endExpression=transformation_expression END_COMMAND
    commands=command*
    END LOOP END_COMMAND;

do_if_command:
    DO IF condition END_COMMAND
    commands=command*
    (ELSE IF condition END_COMMAND elseIfCommands=command*)*
    (ELSE END_COMMAND elseCommands=command*)?
    END IF END_COMMAND;

if_command:
    IF condition variable=IDENTIFIER EQUAL expression=transformation_expression END_COMMAND;

select_if_command: SELECT IF condition END_COMMAND;

delete_variables_command: DELETE VARIABLES (var_list_with_range) END_COMMAND;

rename_command: RENAME VARIABLES (SLASH? OPEN_PAREN? pairs=rename_pair CLOSE_PAREN?)+ END_COMMAND;
rename_pair: oldname=IDENTIFIER EQUAL newname=IDENTIFIER;

aggregate_command: 
    AGGREGATE
    (SLASH? OUTFILE EQUAL (datasetName=STRING | STAR) )?
    (SLASH? MODE EQUAL (REPLACE | ADDVARIABLES) )?
    (SLASH MISSING EQUAL COLUMNWISE)?
    (SLASH DOCUMENT)?
    (SLASH PRESORTED)?
    (SLASH BREAK EQUAL var_list)?
    aggregateVariableDefinition*
    END_COMMAND;

aggregateVariableDefinition:
    SLASH 
    aggregateVariableName=IDENTIFIER 
    (OPEN_SQUARE aggregateVariableLabel=STRING CLOSE_SQUARE)? 
    EQUAL 
    function_call;

numeric_input_value: NUMERIC_LITERAL | LO | LOWEST | HI | HIGHEST | MISSING | SYSMIS | ELSE;
thru: lowValue=numeric_input_value THRU highValue=numeric_input_value;
numeric_value_list_member: numeric_input_value | thru;
numeric_value_list: numeric_value_list_member (COMMA? numeric_value_list_member)*;

string_input_value: STRING | CONVERT | ELSE;
string_value_list: string_input_value (COMMA? string_input_value)*;

numeric_output: NUMERIC_LITERAL | COPY | SYSMIS;
string_output: STRING | COPY;


// Expressions
transformation_expression: 
    NUMERIC_LITERAL
    | STRING
    | IDENTIFIER
    | transformation_expression STARSTAR transformation_expression
    | transformation_expression STAR transformation_expression
    | transformation_expression SLASH transformation_expression
    | transformation_expression PLUS transformation_expression
    | transformation_expression MINUS transformation_expression
    | function_call
    | OPEN_PAREN transformation_expression CLOSE_PAREN
    ;

condition:
    function_call
    | condition (AND | AMPERSAND) condition
    | condition (OR | PIPE) condition
    | NOT condition
    | OPEN_PAREN condition CLOSE_PAREN
    | transformation_expression (EQUAL | EQ) transformation_expression
    | transformation_expression (NOTEQUAL | NOTEQUAL2) transformation_expression
    | transformation_expression (GT | GT2) transformation_expression
    | transformation_expression (GT_EQ | GE) transformation_expression
    | transformation_expression (LT | LT2) transformation_expression
    | transformation_expression (LT_EQ | LE) transformation_expression
    ;

function_call: IDENTIFIER OPEN_PAREN parameter_list CLOSE_PAREN;
parameter_list: parameter (COMMA parameter)*;
parameter:
    transformation_expression
    | var_list_with_range
    ;

var_list: IDENTIFIER+;
var_range: startVariable=IDENTIFIER TO endVariable=IDENTIFIER;
var_list_with_range: (IDENTIFIER | var_range)+;

sort_cases_command: SORT CASES BY? var_list (OPEN_PAREN IDENTIFIER CLOSE_PAREN)? END_COMMAND;

comment_command: SINGLE_LINE_COMMENT | COMMENT_PREFIXED_COMMENT;

frequencies_command: (FREQ | FREQUENCIES) .*? END_COMMAND;
alter_command: ALTER .*? END_COMMAND;
cases_to_vars_command: CASESTOVARS .*? END_COMMAND;
count_command: COUNT .*? END_COMMAND;
list_variables_command: LIST VARIABLES? EQUAL? var_list_with_range END_COMMAND;
temporary_command: TEMPORARY END_COMMAND;
data_list_command: DATA LIST .*? END_COMMAND;
begin_data_command: BEGIN DATA .*? END_COMMAND; 
new_file_command: NEW FILE END_COMMAND;
input_program_command: INPUT PROGRAM END_COMMAND .*? END INPUT PROGRAM END_COMMAND;
sort_variables_command: SORT VARIABLES .*? END_COMMAND;
means_command: MEANS .*? END_COMMAND;
crosstabs_command: CROSSTABS .*? END_COMMAND;
cd_command: CD fileName=STRING END_COMMAND;
define_command: DEFINE .* ENDDEFINE END_COMMAND;
function_call_command: IDENTIFIER END_COMMAND;
set_mprint_command: SET .*? END_COMMAND;
empty_command: END_COMMAND;





// ***************************************************************
// Lexer Rules
// ***************************************************************


// Strings can use either single or double quotes.
// Strings are escaped by two quotes in a row.
STRING : '"' (~[\n"] | '""')* '"' 
       | '\'' (~[\n\'] | '\'\'')* '\'';

END_COMMAND: DOT+ [ \t]* { _input.La(1) == '\n'}?
    | DOT [ \t]* '/*' ~[\n]* ;

NUMERIC_LITERAL: 
      MINUS DIGIT+ (DOT DIGIT+)?
    | DIGIT+ (DOT DIGIT+)?
    | DOT DIGIT+;

// A comment that begins with a * ends either 
//     * with a blank line (which could include white space, or 
//     * a period at the end of the line, possibly followed by whitespace.
//     * the end of the file.
SINGLE_LINE_COMMENT: 
    '\n' + [ \t]* '*' .*? ('\n\n' | EOF | '.' [ \t]* { _input.La(1) == '\n'}?)
    ;

COMMENT_PREFIXED_COMMENT:
    'comment ' ~[\n]*
    ;

SINGLE_LINE_COMMENT2
 : '/*' ~[\n]* -> channel(HIDDEN)
 ;




NEWLINE : [\n\u2028\u2029]+ -> skip;
ADD: A D D;
ADDVARIABLES: A D D V A R I A B L E S;
AGGREGATE: A G G R E G A T E;
ALTER: A L T E R;
AND: A N D;
BEGIN: B E G I N;
BREAK: B R E A K;
BY: B Y;
CASES: C A S E S;
CASESTOVARS: C A S E S T O V A R S;
CD: C D;
COLUMNWISE: C O L U M N W I S E;
COMPRESSED: C O M P R E S S E D;
COMPUTE: C O M P U T E;
CONVERT: C O N V E R T;
COPY: C O P Y;
COUNT: C O U N T;
CROSSTABS: C R O S S T A B S;
DATA: D A T A;
DEFINE: D E F I N E;
DELETE: D E L E T E;
DO: D O;
DOCUMENT: D O C U M E N T;
ELSE: E L S E;
END: E N D;
ENDDEFINE: '!' E N D D E F I N E;
EQ : E Q;
EXECUTE: E X E C U T E;
FILE: F I L E;
FILES: F I L E S;
FORMAT: F O R M A T;
FORMATS: F O R M A T S;
FREQ: F R E Q;
FREQUENCIES: F R E Q U E N C I E S;
GE: G E;
GET: G E T;
GT2 : G T;
HI: H I;
HIGHEST: H I G H E S T;
IF: I F;
IN: I N;
INPUT: I N P U T;
INTO: I N T O;
KEEP: K E E P;
LABEL: L A B E L;
LABELS: L A B E L S;
LE: L E;
LIMIT: L I M I T;
LIST: L I S T;
LO: L O;
LOOP: L O O P;
LOWEST: L O W E S T;
LT2 : L T;
MATCH : M A T C H;
MEANS : M E A N S;
MODE : M O D E;
MPRINT : M P R I N T;
MISSING: M I S S I N G;
NEW: N E W;
NO: N O;
NOTEQUAL2 : N E;
NOT: N O T;
OR: O R;
OUTFILE: O U T F I L E;
PRESORTED: P R E S O R T E D;
PRINT: P R I N T;
PROGRAM: P R O G R A M;
RECODE: R E C O D E;
RENAME: R E N A M E;
REPEAT: R E P E A T;
REPLACE: R E P L A C E;
SAVE: S A V E;
SELECT: S E L E C T;
SET: S E T;
SORT: S O R T;
SUBTITLE: S U B T I T L E;
SYSMIS: S Y S M I S;
TEMPORARY: T E M P O R A R Y;
THRU: T H R U;
TITLE: T I T L E;
TO: T O;
UNCOMPRESSED: U N C O M P R E S S E D;
VALUE: V A L U E;
VALUES: V A L U E S;
VAR: V A R;
VARIABLE: V A R I A B L E;
VARS: V A R S;
VARIABLES: V A R I A B L E S;

IDENTIFIER: [a-zA-Z_@#] [a-zA-Z_@0-9]* (DOT [a-zA-Z_@0-9]+)? ;

DOT : '.';
SLASH : '/';
COMMA : ',';
SEMICOLON : ';';
OPEN_PAREN : '(';
CLOSE_PAREN : ')';
OPEN_SQUARE : '[';
CLOSE_SQUARE : ']';
EQUAL : '=';
NOTEQUAL : '<>';
STAR : '*';
STARSTAR : '**';
PLUS : '+';
MINUS : '-';
TILDE : '~';
LT : '<';
LT_EQ : '<=';
GT : '>';
GT_EQ : '>=';
PIPE : '|';
AMPERSAND : '&';

DIGIT : [0-9];

fragment A : [aA];
fragment B : [bB];
fragment C : [cC];
fragment D : [dD];
fragment E : [eE];
fragment F : [fF];
fragment G : [gG];
fragment H : [hH];
fragment I : [iI];
fragment J : [jJ];
fragment K : [kK];
fragment L : [lL];
fragment M : [mM];
fragment N : [nN];
fragment O : [oO];
fragment P : [pP];
fragment Q : [qQ];
fragment R : [rR];
fragment S : [sS];
fragment T : [tT];
fragment U : [uU];
fragment V : [vV];
fragment W : [wW];
fragment X : [xX];
fragment Y : [yY];
fragment Z : [zZ];

SPACE : ' ' -> skip;
TAB : '\t' -> skip;