﻿using C2Metadata.Common.SpssConverter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace C2Metadata.SpssToSdtl.Tests
{
    public class ConditionTests
    {
        private ITestOutputHelper output;

        public ConditionTests(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void SelectIf_Simple()
        {
            var converter = new SpssConverter();
            var program = converter.ConvertString("SELECT IF (A1 <> 1).");
            this.output.WriteLine("Messages:\n================\n" + string.Join("\n\n", program.Messages.Select(x => $"{x.LineNumber}:{x.CharacterPosition} - {x.MessageText}")));
            Assert.Empty(program.Messages);
        }

        [Fact]
        public void SelectIf_And()
        {
            var converter = new SpssConverter();
            var program = converter.ConvertString("SELECT IF (A1 <> 1 AND A2 <> 1).");
            this.output.WriteLine("Messages:\n================\n" + string.Join("\n\n", program.Messages.Select(x => $"{x.LineNumber}:{x.CharacterPosition} - {x.MessageText}")));
            Assert.Empty(program.Messages);
        }

        [Fact]
        public void SelectIf_WithNeNotSymbols()
        {
            var converter = new SpssConverter();
            var program = converter.ConvertString("SELECT IF (A1 NE 1).");
            this.output.WriteLine("Messages:\n================\n" + string.Join("\n\n", program.Messages.Select(x => $"{x.LineNumber}:{x.CharacterPosition} - {x.MessageText}")));
            Assert.Empty(program.Messages);
        }

        [Fact]
        public void SelectIf_WithParentheses()
        {
            var converter = new SpssConverter();
            var program = converter.ConvertString("SELECT IF (A1 NE 1 AND A2 NE 999999999).");
            this.output.WriteLine("Messages:\n================\n" + string.Join("\n\n", program.Messages.Select(x => $"{x.LineNumber}:{x.CharacterPosition} - {x.MessageText}")));
            Assert.Empty(program.Messages);
        }

        [Fact]
        public void SelectIf_WithAtInIdentifier()
        {
            var converter = new SpssConverter();
            var program = converter.ConvertString("SELECT IF (AA2i@i NE 1 AND AA2i@i1 NE 999999999).");
            this.output.WriteLine("Messages:\n================\n" + string.Join("\n\n", program.Messages.Select(x => $"{x.LineNumber}:{x.CharacterPosition} - {x.MessageText}")));
            Assert.Empty(program.Messages);
        }

        [Fact]
        public void DoIf_WithHyphensAtBeginningOfLine()
        {
            var converter = new SpssConverter();
            var program = converter.ConvertString(@"Do if (C1PA9 eq 3).
- Compute C1PHRTRS= 3.
Else if (C1PA9 eq 1 and C1PA9a eq 1).
- Compute C1PHRTRS= 6.
Else if (C1PA9 eq 1 and C1PA9a eq 2).
- Compute C1PHRTRS= 5.
Else if (C1PA9 eq 1 and C1PA9a eq 3).
- Compute C1PHRTRS= 4.
Else if (C1PA9 eq 2 and C1PA9b eq 1).
- Compute C1PHRTRS= 0.
Else if (C1PA9 eq 2 and C1PA9b eq 2).
- Compute C1PHRTRS= 1.
Else if (C1PA9 eq 2 and C1PA9b eq 3).
- Compute C1PHRTRS= 2.
End if.
");
            this.output.WriteLine("Messages:\n================\n" + string.Join("\n\n", program.Messages.Select(x => $"{x.LineNumber}:{x.CharacterPosition} - {x.MessageText}")));
            Assert.Empty(program.Messages);
        }

        [Fact]
        public void Snippet_Test()
        {
            var converter = new SpssConverter();
            var program = converter.ConvertString(@"

* *********************************************************************************************************************************.
* Even though F7 and F8 are 'check all that apply' question, we will not convert them into dummy variables as other 'CATA'
and keep their format consistent with M2. (11 - 27 - 2012)

EXECUTE.




");
            this.output.WriteLine("Messages:\n================\n" + string.Join("\n\n", program.Messages.Select(x => $"{x.LineNumber}:{x.CharacterPosition} - {x.MessageText}")));
            Assert.Empty(program.Messages);
        }

    }
}
