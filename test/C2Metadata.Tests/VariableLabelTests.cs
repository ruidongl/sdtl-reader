﻿using C2Metadata.Common.SpssConverter;
using sdtl;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace C2Metadata.SpssToSdtl.Tests
{
    public class VariableLabelTests
    {
        [Fact]
        public void VariableLabel_UseCase1()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("Variable labels Partycare1 'Care who wins elections - Index 1'.");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            Assert.IsType<SetVariableLabel>(commandList.Commands[0]);
            var command = commandList.Commands[0] as SetVariableLabel;

            var varSymbol = command.Variable as VariableSymbolExpression;
            Assert.Equal("Partycare1", varSymbol.VariableName);
            Assert.Equal("Care who wins elections - Index 1", command.Label);
        }


    }
}
