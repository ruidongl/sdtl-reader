﻿using C2Metadata.Common.RConverter;
using C2Metadata.SpssToSdtl.Tests.Utility;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace C2Metadata.SpssToSdtl.Tests.R
{
    public class UseCaseTests
    {
        private ITestOutputHelper output;

        public UseCaseTests(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void RTest1()
        {
            string fileName = @"c:/svn/r-examples/dplyr-example.r";
            this.output.WriteLine("Testing " + fileName);

            var converter = new RConverter();
            var program = converter.ConvertFile(fileName, true);
            Assert.NotNull(program);

            int lines = program.Commands.Count;
            this.output.WriteLine($"{lines} lines");

            TestHelper.SaveJson(program, @"c:\out\r1.sdtl.json");

        }

        [Fact]
        public void RTest2()
        {
            string fileName = @"c:/svn/r-examples/recode/Recode_2ways.R";
            this.output.WriteLine("Testing " + fileName);

            var converter = new RConverter();
            var program = converter.ConvertFile(fileName, true);
            Assert.NotNull(program);

            int lines = program.Commands.Count;
            this.output.WriteLine($"{lines} lines");

            TestHelper.SaveJson(program, @"c:\out\r2.sdtl.json");

        }
    }
}
