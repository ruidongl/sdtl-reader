﻿using C2Metadata.Common.SpssConverter;
using C2Metadata.SpssToSdtl.Tests.Utility;
using sdtl;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace C2Metadata.SpssToSdtl.Tests
{
    public class MissingValuesTests
    {
        [Fact]
        public void MissingValues_UseCase1()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("missing values SEXRACE (0).");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            Assert.IsType<SetMissingValues>(commandList.Commands[0]);
            var command = commandList.Commands[0] as SetMissingValues;

            Assert.Single(command.Variables);
            TestHelper.AssertSingleVariableWithName(command.Variables, "SEXRACE");

            Assert.Equal("0", (command.Values[0] as NumericConstantExpression).Value);
        }

        [Fact]
        public void MissingValues_UseCase2()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("missing values V520041 (8 thru hi) V520042 V520043 (8 thru hi, 0).");
            int count = commandList.Commands.Count;
            Assert.Equal(2, count);

            Assert.IsType<SetMissingValues>(commandList.Commands[0]);
            var command1 = commandList.Commands[0] as SetMissingValues;
            var command2 = commandList.Commands[1] as SetMissingValues;

            TestHelper.AssertSingleVariableWithName(command1.Variables, "V520041");

            var range1 = command1.Values[0] as NumberRangeExpression;
            Assert.Equal("8", (range1.NumberRangeStart as NumericConstantExpression).Value);
            Assert.IsType<NumericMaximumValueExpression>(range1.NumberRangeEnd);

            Assert.IsType<VariableSymbolExpression>(command2.Variables[0]);
            Assert.IsType<VariableSymbolExpression>(command2.Variables[1]);
            var varSymbol0 = command2.Variables[0] as VariableSymbolExpression;
            var varSymbol1 = command2.Variables[1] as VariableSymbolExpression;

            Assert.Equal("V520042", varSymbol0.VariableName);
            Assert.Equal("V520043", varSymbol1.VariableName);

            var range2 = command2.Values[0] as NumberRangeExpression;
            Assert.Equal("8", (range2.NumberRangeStart as NumericConstantExpression).Value);
            Assert.IsType<NumericMaximumValueExpression>(range2.NumberRangeEnd);
            Assert.Equal("0", (command2.Values[1] as NumericConstantExpression).Value);

        }
    }
}
