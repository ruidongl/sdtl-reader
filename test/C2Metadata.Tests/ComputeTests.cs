﻿using C2Metadata.Common.SpssConverter;
using C2Metadata.SpssToSdtl.Tests.Utility;
using sdtl;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace C2Metadata.SpssToSdtl.Tests
{
    public class ComputeTests
    {
        [Fact]
        public void ComputeConstantNumeric()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("COMPUTE var1 = 10.");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            Assert.IsType<Compute>(commandList.Commands[0]);
            var compute = commandList.Commands[0] as Compute;

            TestHelper.AssertSingleVariableWithName(compute.Variable, "var1");
            Assert.IsType<NumericConstantExpression>(compute.Expression);

            var expr = compute.Expression as NumericConstantExpression;
            Assert.Equal("10", expr.Value);
        }

        [Fact]
        public void ComputeConstantString()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("COMPUTE var1 = \"Hello, world.\".");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            Assert.IsType<Compute>(commandList.Commands[0]);
            var compute = commandList.Commands[0] as Compute;

            TestHelper.AssertSingleVariableWithName(compute.Variable, "var1");
            Assert.IsType<StringConstantExpression>(compute.Expression);

            var expr = compute.Expression as StringConstantExpression;
            Assert.Equal("Hello, world.", expr.Value);
        }

        [Fact]
        public void ComputeConstantWithParens()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("COMPUTE var1 = (10).");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            Assert.IsType<Compute>(commandList.Commands[0]);
            var compute = commandList.Commands[0] as Compute;

            TestHelper.AssertSingleVariableWithName(compute.Variable, "var1");
            Assert.IsType<GroupedExpression>(compute.Expression);

            var expr = compute.Expression as GroupedExpression;

            Assert.IsType<NumericConstantExpression>(expr.Expression);
            var intExpr = expr.Expression as NumericConstantExpression;
            Assert.Equal("10", intExpr.Value);
        }

        [Fact]
        public void ComputeArithmeticAdd()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("COMPUTE var1 = 2 + 3.");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            Assert.IsType<Compute>(commandList.Commands[0]);
            var compute = commandList.Commands[0] as Compute;

            TestHelper.AssertSingleVariableWithName(compute.Variable, "var1");
            Assert.IsType<FunctionCallExpression>(compute.Expression);

            var expr = compute.Expression as FunctionCallExpression;

            Assert.IsType<NumericConstantExpression>(expr.Arguments[0].ArgumentValue);
            Assert.IsType<NumericConstantExpression>(expr.Arguments[1].ArgumentValue);

            var term1 = expr.Arguments[0].ArgumentValue as NumericConstantExpression;
            var term2 = expr.Arguments[1].ArgumentValue as NumericConstantExpression;

            Assert.Equal("2", term1.Value);
            Assert.Equal("3", term2.Value);
        }

        [Fact]
        public void ComputeArithmeticSubtract()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("COMPUTE var1 = 2 - 3.");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            var compute = commandList.Commands[0] as Compute;

            TestHelper.AssertSingleVariableWithName(compute.Variable, "var1");
            Assert.IsType<FunctionCallExpression>(compute.Expression);

            var expr = compute.Expression as FunctionCallExpression;

            Assert.IsType<NumericConstantExpression>(expr.Arguments[0].ArgumentValue);
            Assert.IsType<NumericConstantExpression>(expr.Arguments[1].ArgumentValue);

            var term1 = expr.Arguments[0].ArgumentValue as NumericConstantExpression;
            var term2 = expr.Arguments[1].ArgumentValue as NumericConstantExpression;

            Assert.Equal("2", term1.Value);
            Assert.Equal("3", term2.Value);
        }

        [Fact]
        public void ComputeArithmeticMultiply()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("COMPUTE var1 = 2 * 3.");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            var compute = commandList.Commands[0] as Compute;

            TestHelper.AssertSingleVariableWithName(compute.Variable, "var1");
            Assert.IsType<FunctionCallExpression>(compute.Expression);

            var expr = compute.Expression as FunctionCallExpression;

            Assert.IsType<NumericConstantExpression>(expr.Arguments[0].ArgumentValue);
            Assert.IsType<NumericConstantExpression>(expr.Arguments[1].ArgumentValue);

            var term1 = expr.Arguments[0].ArgumentValue as NumericConstantExpression;
            var term2 = expr.Arguments[1].ArgumentValue as NumericConstantExpression;

            Assert.Equal("2", term1.Value);
            Assert.Equal("3", term2.Value);
        }

        [Fact]
        public void ComputeArithmeticDivide()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("COMPUTE var1 = 10 / 2.");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            var compute = commandList.Commands[0] as Compute;

            TestHelper.AssertSingleVariableWithName(compute.Variable, "var1");
            Assert.IsType<FunctionCallExpression>(compute.Expression);

            var expr = compute.Expression as FunctionCallExpression;

            Assert.IsType<NumericConstantExpression>(expr.Arguments[0].ArgumentValue);
            Assert.IsType<NumericConstantExpression>(expr.Arguments[1].ArgumentValue);

            var term1 = expr.Arguments[0].ArgumentValue as NumericConstantExpression;
            var term2 = expr.Arguments[1].ArgumentValue as NumericConstantExpression;

            Assert.Equal("10", term1.Value);
            Assert.Equal("2", term2.Value);
        }

        [Fact]
        public void ComputeArithmeticExponent()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("COMPUTE var1 = 2 ** 16.");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);
            
            var compute = commandList.Commands[0] as Compute;

            TestHelper.AssertSingleVariableWithName(compute.Variable, "var1");
            Assert.IsType<FunctionCallExpression>(compute.Expression);

            var expr = compute.Expression as FunctionCallExpression;

            Assert.IsType<NumericConstantExpression>(expr.Arguments[0].ArgumentValue);
            Assert.IsType<NumericConstantExpression>(expr.Arguments[1].ArgumentValue);

            var term1 = expr.Arguments[0].ArgumentValue as NumericConstantExpression;
            var term2 = expr.Arguments[1].ArgumentValue as NumericConstantExpression;

            Assert.Equal("2", term1.Value);
            Assert.Equal("16", term2.Value);
        }

        [Fact]
        public void ComputeArithmeticComplex1()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("COMPUTE var1 = (2 + 4) - 10 * 2**3.");
            Assert.Single(commandList.Commands);

            // The correct tree here is:
            // SUBTRACT
            //   GROUP
            //     ADD
            //       2
            //       4
            //   MULTIPLY
            //     10
            //     EXPONENT
            //       2
            //       3

            // Compute command with var1 as the variable.
            Assert.IsType<Compute>(commandList.Commands[0]);
            var compute = commandList.Commands[0] as Compute;
            TestHelper.AssertSingleVariableWithName(compute.Variable, "var1");

            // Subtraction of the Group minus all the other stuff
            Assert.IsType<FunctionCallExpression>(compute.Expression);
            var subtraction = compute.Expression as FunctionCallExpression;

            // Subtraction left side: Grouped Addition of 2 + 4
            Assert.IsType<GroupedExpression>(subtraction.Arguments[0].ArgumentValue);
            var group = subtraction.Arguments[0].ArgumentValue as GroupedExpression;
            Assert.IsType<FunctionCallExpression>(group.Expression);
            var addition = group.Expression as FunctionCallExpression;

            // 2
            Assert.IsType<NumericConstantExpression>(addition.Arguments[0].ArgumentValue);
            var intLit1 = addition.Arguments[0].ArgumentValue as NumericConstantExpression;
            Assert.Equal("2", intLit1.Value);

            // 4
            Assert.IsType<NumericConstantExpression>(addition.Arguments[1].ArgumentValue);
            var intLit2 = addition.Arguments[1].ArgumentValue as NumericConstantExpression;
            Assert.Equal("4", intLit2.Value);

            // Subtraction right side: multiplication of 10 * 2**8
            Assert.IsType<FunctionCallExpression>(subtraction.Arguments[1].ArgumentValue);
            var multiplication = subtraction.Arguments[1].ArgumentValue as FunctionCallExpression;
            
            // Multiplication left side: 10
            Assert.IsType<NumericConstantExpression>(multiplication.Arguments[0].ArgumentValue);
            var intLit3 = multiplication.Arguments[0].ArgumentValue as NumericConstantExpression;
            Assert.Equal("10", intLit3.Value);

            // Multiplication right side: 2**3
            Assert.IsType<FunctionCallExpression>(multiplication.Arguments[1].ArgumentValue);
            var exponent = multiplication.Arguments[1].ArgumentValue as FunctionCallExpression;

            // 2
            Assert.IsType<NumericConstantExpression>(exponent.Arguments[0].ArgumentValue);
            var intLit4 = exponent.Arguments[0].ArgumentValue as NumericConstantExpression;
            Assert.Equal("2", intLit4.Value);

            // 3
            Assert.IsType<NumericConstantExpression>(exponent.Arguments[1].ArgumentValue);
            var intLit5 = exponent.Arguments[1].ArgumentValue as NumericConstantExpression;
            Assert.Equal("3", intLit5.Value);
        }

        [Fact]
        public void ComputeArithmeticComplex2()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("COMPUTE var1 = (2 + var2) - 10 * 2**var3.");
            Assert.Single(commandList.Commands);

            // Compute command with var1 as the variable.
            Assert.IsType<Compute>(commandList.Commands[0]);
            var compute = commandList.Commands[0] as Compute;
            TestHelper.AssertSingleVariableWithName(compute.Variable, "var1");

            // Subtraction of the Group minus all the other stuff
            Assert.IsType<FunctionCallExpression>(compute.Expression);
            var subtraction = compute.Expression as FunctionCallExpression;

            // Subtraction left side: Grouped Addition of 2 + var2
            Assert.IsType<GroupedExpression>(subtraction.Arguments[0].ArgumentValue);
            var group = subtraction.Arguments[0].ArgumentValue as GroupedExpression;
            Assert.IsType<FunctionCallExpression>(group.Expression);
            var addition = group.Expression as FunctionCallExpression;

            // 2
            Assert.IsType<NumericConstantExpression>(addition.Arguments[0].ArgumentValue);
            var intLit1 = addition.Arguments[0].ArgumentValue as NumericConstantExpression;
            Assert.Equal("2", intLit1.Value);

            // var2
            Assert.IsType<VariableSymbolExpression>(addition.Arguments[1].ArgumentValue);
            var symbol1 = addition.Arguments[1].ArgumentValue as VariableSymbolExpression;
            Assert.Equal("var2", symbol1.VariableName);

            // Subtraction right side: multiplication of 10 * 2**var3
            Assert.IsType<FunctionCallExpression>(subtraction.Arguments[1].ArgumentValue);
            var multiplication = subtraction.Arguments[1].ArgumentValue as FunctionCallExpression;
            
            // Multiplication left side: 10
            Assert.IsType<NumericConstantExpression>(multiplication.Arguments[0].ArgumentValue);
            var intLit3 = multiplication.Arguments[0].ArgumentValue as NumericConstantExpression;
            Assert.Equal("10", intLit3.Value);

            // Multiplication right side: 2**var3
            Assert.IsType<FunctionCallExpression>(multiplication.Arguments[1].ArgumentValue);
            var exponent = multiplication.Arguments[1].ArgumentValue as FunctionCallExpression;

            // 2
            Assert.IsType<NumericConstantExpression>(exponent.Arguments[0].ArgumentValue);
            var intLit4 = exponent.Arguments[0].ArgumentValue as NumericConstantExpression;
            Assert.Equal("2", intLit4.Value);

            // var3
            Assert.IsType<VariableSymbolExpression>(exponent.Arguments[1].ArgumentValue);
            var symbol2 = exponent.Arguments[1].ArgumentValue as VariableSymbolExpression;
            Assert.Equal("var3", symbol2.VariableName);
        }

        [Fact]
        public void ComputeCopy()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("COMPUTE var1 = var2.");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            var compute = commandList.Commands[0] as Compute;

            TestHelper.AssertSingleVariableWithName(compute.Variable, "var1");
            Assert.IsType<VariableSymbolExpression>(compute.Expression);

            var expr = compute.Expression as VariableSymbolExpression;

            Assert.Equal("var2", expr.VariableName);
        }

        [Fact]
        public void ComputeFunctionSingleParameter()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("COMPUTE sqrt = SQRT(var4).");
            Assert.Single(commandList.Commands);

            var compute = commandList.Commands[0] as Compute;
            TestHelper.AssertSingleVariableWithName(compute.Variable, "sqrt");
            Assert.IsType<FunctionCallExpression>(compute.Expression);

            var expr = compute.Expression as FunctionCallExpression;

            Assert.Equal("square_root", expr.Function);
            Assert.Single(expr.Arguments);

            var param = expr.Arguments[0];

            Assert.IsType<VariableSymbolExpression>(param.ArgumentValue);
            var paramExpr = param.ArgumentValue as VariableSymbolExpression;
            Assert.Equal("var4", paramExpr.VariableName);
        }

        // COMPUTE C1PIDATE=date.dmy(number(substr(ltrim(date),3,2),f2.0), 
    //number(substr(ltrim(date),1,2),f2.0), number(substr(ltrim(date),5),f4.0)).


        [Fact]
        public void ComputeFunctionMultipleParameters()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("COMPUTE avg = MEAN(var1, var2, var3, var4).");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            var compute = commandList.Commands[0] as Compute;
            TestHelper.AssertSingleVariableWithName(compute.Variable, "avg");
            Assert.IsType<FunctionCallExpression>(compute.Expression);

            var expr = compute.Expression as FunctionCallExpression;

            Assert.Equal("mean", expr.Function);
            Assert.Equal(4, expr.Arguments.Count);

            var param1 = expr.Arguments[0].ArgumentValue as ExpressionBase;
            Assert.IsType<VariableSymbolExpression>(param1);
            var param1Expr = param1 as VariableSymbolExpression;
            Assert.Equal("var1", param1Expr.VariableName);

            var param2 = expr.Arguments[1].ArgumentValue as ExpressionBase;
            Assert.IsType<VariableSymbolExpression>(param2);
            var param2Expr = param2 as VariableSymbolExpression;
            Assert.Equal("var2", param2Expr.VariableName);
        }

        [Fact]
        public void ComputedNestedFunctions()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("COMPUTE sqrt = TRUNC(SQRT(var4)).");
            int count = commandList.Commands.Count;
            Assert.Single(commandList.Commands);

            var compute = commandList.Commands[0] as Compute;
            TestHelper.AssertSingleVariableWithName(compute.Variable, "sqrt");
            Assert.IsType<FunctionCallExpression>(compute.Expression);

            // TRUNC function call
            var truncExpr = compute.Expression as FunctionCallExpression;
            Assert.Equal("truncate", truncExpr.Function);
            Assert.Single(truncExpr.Arguments);

            // TRUNC's parameter is the SQRT function call
            var param = truncExpr.Arguments[0].ArgumentValue as ExpressionBase;
            Assert.IsType<FunctionCallExpression>(param);
            var sqrtExpr = param as FunctionCallExpression;
            Assert.Equal("square_root", sqrtExpr.Function);

            // SQRT's parameter is the symbol var4.
            Assert.Single(sqrtExpr.Arguments);
            var sqrtParam = sqrtExpr.Arguments[0].ArgumentValue as ExpressionBase;
            Assert.IsType<VariableSymbolExpression>(sqrtParam);
            var sqrtSymbolExpression = sqrtParam as VariableSymbolExpression;
            Assert.Equal("var4", sqrtSymbolExpression.VariableName);
        }

        [Fact]
        public void ComputeSumN()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("COMPUTE var5 = SUM.2(var1, var2, var3).");
            int count = commandList.Commands.Count;
            Assert.Single(commandList.Commands);

            var compute = commandList.Commands[0] as Compute;
            TestHelper.AssertSingleVariableWithName(compute.Variable, "var5");

            Assert.IsType<FunctionCallExpression>(compute.Expression);
            var expr = compute.Expression as FunctionCallExpression;
            Assert.Equal("sum", expr.Function);

            Assert.Equal(3, expr.Arguments.Count);

            var param = expr.Arguments[0].ArgumentValue as ExpressionBase;
            Assert.IsType<VariableSymbolExpression>(param);
            var paramExpr = param as VariableSymbolExpression;
            Assert.Equal("var1", paramExpr.VariableName);
        }

        [Fact]
        public void FunctionCallWithVariableRange()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("COMPUTE var5 = MEAN(var1 TO var3).");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);
        }

        [Fact]
        public void FunctionCallWithVariableRange2()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("compute Partycare2 = sum(V520041 to V520043).");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            Assert.IsType<Compute>(commandList.Commands[0]);

            var compute = commandList.Commands[0] as Compute;

            TestHelper.AssertSingleVariableWithName(compute.Variable, "Partycare2");
            Assert.IsType<FunctionCallExpression>(compute.Expression);
            var functionCall = compute.Expression as FunctionCallExpression;

            Assert.Single(functionCall.Arguments);
            Assert.IsType<VariableListExpression>(functionCall.Arguments[0].ArgumentValue);

            var list = functionCall.Arguments[0].ArgumentValue as VariableListExpression;
            var range = list.Variables[0] as VariableRangeExpression;
            Assert.Equal("V520041", range.First);
            Assert.Equal("V520043", range.Last);

        }
    }
}
