﻿using C2Metadata.Common.SpssConverter;
using sdtl;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace C2Metadata.SpssToSdtl.Tests
{
    public class CommentTests
    {
        [Fact]
        public void SingleLineComment()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("* This is a simple, single line comment.");
            Assert.Single(commandList.Commands);
        }

        [Fact]
        public void SingleLineCommentWithOtherCommandsFollowing()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString(@"* This is a simple, single line comment.


COMPUTE var1 = 10.");
            Assert.Equal(2, commandList.Commands.Count);

            var comment = commandList.Commands[0] as Comment;
            Assert.Equal("This is a simple, single line comment.", comment.CommentText);
        }

        [Fact]
        public void SingleLineSlashCommentWithOtherCommandsFollowing()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString(@"/* This is a simple, single line comment.

COMPUTE var1 = 10.");
            Assert.Single(commandList.Commands);
        }

        [Fact]
        public void EndOfLineComment()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString(@"COMPUTE var1 = 10. /* Here is the comment");
            Assert.Single(commandList.Commands);
        }

        [Fact]
        public void SingleLineCommentWithNoTerminator()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString("* This is a simple, single line comment");
            Assert.Single(commandList.Commands);
        }

        [Fact]
        public void LineOfStars()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString(@"******************************
/* just a test");
            Assert.Single(commandList.Commands);
        }

        [Fact]
        public void DoubleAsterisk()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString(@"**RECODE var1 (SYSMIS=99).**


*******test***************.");
            Assert.Equal(2, commandList.Commands.Count);
        }

        [Fact]
        public void Comment_Before_Command()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString(@"***************.
compute age_rec = age.");
            Assert.Equal(2, commandList.CommandCount);
            Assert.IsType<Comment>(commandList.Commands[0]);
            Assert.IsType<Compute>(commandList.Commands[1]);
        }

        [Fact]
        public void Comment_After_Command()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString(@"compute age_rec=age.
***************.");
            Assert.Equal(2, commandList.CommandCount);
            Assert.IsType<Compute>(commandList.Commands[0]);
            Assert.IsType<Comment>(commandList.Commands[1]);
        }

        [Fact]
        public void Comment_On_First_Line_With_Invalid_Spss()
        {
            var converter = new SpssConverter();
            var commandList = converter.ConvertString(@"* Encoding: UTF-8.

GET   FILE='36797-0001-Data-C2M.sav'.

comment  **********  INVALID SPSS **********************.
compute age_rec==age.

RECODE age (15 thru 29.999) (30 thru 49.999=30) (50 thru Highest=50).


VARIABLE LABELS ""Age recoded by RECODE"".


RENAME VARIABLES(age_rec = age_3cat).

");
            Assert.Equal(7, commandList.CommandCount);
            Assert.Equal(3, commandList.Messages.Count);
            //Assert.IsType<Compute>(commandList.Commands[0]);
            //Assert.IsType<Comment>(commandList.Commands[1]);
        }
    }
}
