﻿using C2Metadata.Common.SpssConverter;
using C2Metadata.SpssToSdtl.Tests.Utility;
using sdtl;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace C2Metadata.SpssToSdtl.Tests
{
    public class UseCaseTests
    {
        private ITestOutputHelper output;

        public static IEnumerable<object[]> Paths
        {
            get
            {
                var paths = FileSystemHelpers.GatherSpssCommandFiles(@"c:/svn/use_cases");

                foreach (string path in paths)
                {
                    yield return new object[] { path };
                }
            }
        }

        public UseCaseTests(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Theory]
        [MemberData(nameof(Paths))]
        public void RunConverterOnUseCaseExample(string fileName)
        {
            this.output.WriteLine("Testing " + fileName);

            var converter = new SpssConverter();
            var program = converter.ConvertFile(fileName, true);
            Assert.NotNull(program);

            int lines = program.Commands.Count;
            this.output.WriteLine($"{lines} lines");
        }

        [Fact]
        public void Mvp_Test_1()
        {
            string fileName = @"c:/svn/use_cases/SPSS_full_MVP_scripts/gss_mvp_v01.sps";
            this.output.WriteLine("Testing " + fileName);

            var converter = new SpssConverter();
            var program = converter.ConvertFile(fileName, true);
            Assert.NotNull(program);

            int lines = program.Commands.Count;
            this.output.WriteLine($"{lines} lines");

            TestHelper.SaveJson(program, @"c:\out\gss_mvp_01.sdtl.json");

            Assert.Equal(51, lines);
            Assert.IsType<Comment>(program.Commands[0]);
            Assert.IsType<Load>(program.Commands[1]);
            Assert.IsType<Comment>(program.Commands[2]);
            Assert.IsType<Compute>(program.Commands[3]);
            Assert.IsType<Recode>(program.Commands[4]);
            Assert.IsType<SetVariableLabel>(program.Commands[5]);
            Assert.IsType<SetValueLabels>(program.Commands[6]);
            Assert.IsType<Rename>(program.Commands[7]);
            Assert.IsType<Comment>(program.Commands[8]);
            Assert.IsType<Compute>(program.Commands[9]);
            Assert.IsType<SetVariableLabel>(program.Commands[10]);
            Assert.IsType<Comment>(program.Commands[11]);
            Assert.IsType<Analysis>(program.Commands[12]);
            Assert.IsType<Comment>(program.Commands[13]);
            Assert.IsType<Compute>(program.Commands[14]);
            Assert.IsType<Analysis>(program.Commands[15]);
            Assert.IsType<Comment>(program.Commands[16]);
            Assert.IsType<Select>(program.Commands[17]);
            Assert.IsType<Select>(program.Commands[18]);
            Assert.IsType<Analysis>(program.Commands[19]);
            Assert.IsType<Comment>(program.Commands[20]);
            Assert.IsType<DeleteVariables>(program.Commands[21]);
            Assert.IsType<DeleteVariables>(program.Commands[22]);
            Assert.IsType<Comment>(program.Commands[23]);
            Assert.IsType<Analysis>(program.Commands[24]);
            Assert.IsType<SetMissingValues>(program.Commands[25]);
            Assert.IsType<Analysis>(program.Commands[26]);
            Assert.IsType<Comment>(program.Commands[27]);
            Assert.IsType<Comment>(program.Commands[28]);
            Assert.IsType<Compute>(program.Commands[29]);
            Assert.IsType<Compute>(program.Commands[30]);
            Assert.IsType<Compute>(program.Commands[31]);
            Assert.IsType<Compute>(program.Commands[32]);
            Assert.IsType<Compute>(program.Commands[33]);
            Assert.IsType<Unsupported>(program.Commands[34]); // SORT VARIABLES by NAME
            Assert.IsType<Comment>(program.Commands[35]);
            Assert.IsType<DeleteVariables>(program.Commands[36]);
            Assert.IsType<Comment>(program.Commands[37]);
            Assert.IsType<Aggregate>(program.Commands[38]); // AGGREGATE ...
            Assert.IsType<Analysis>(program.Commands[39]); // FREQUENCIES ...
            Assert.IsType<Analysis>(program.Commands[40]); // MEANS ...
            Assert.IsType<Comment>(program.Commands[41]);
            Assert.IsType<Compute>(program.Commands[42]);
            Assert.IsType<Compute>(program.Commands[43]);
            Assert.IsType<LoopOverList>(program.Commands[44]); // COMPUTE (within loop?)
            Assert.IsType<Analysis>(program.Commands[45]); // CROSSTABS ...
            Assert.IsType<Comment>(program.Commands[46]);
            Assert.IsType<Compute>(program.Commands[47]);
            Assert.IsType<SetMissingValues>(program.Commands[48]);
            Assert.IsType<LoopOverList>(program.Commands[49]); // LOOP ...
            Assert.IsType<Analysis>(program.Commands[50]); // CROSSTABS ...

        }

        [Fact]
        public void Mvp_Multi_Test_1()
        {
            string fileName = @"c:/svn/use_cases/SPSS_full_MVP_scripts/gss_mvp_multi_v01.sps";
            this.output.WriteLine("Testing " + fileName);

            var converter = new SpssConverter();
            var program = converter.ConvertFile(fileName, true);
            Assert.NotNull(program);

            int lines = program.Commands.Count;
            this.output.WriteLine($"{lines} lines");

            TestHelper.SaveJson(program, @"c:\out\mvp_multi_test.sdtl.json");

            Assert.IsType<Comment>(program.Commands[0]);
            Assert.IsType<Load>(program.Commands[1]);
            Assert.IsType<Analysis>(program.Commands[2]); // FREQUENCIES region.
            Assert.IsType<Comment>(program.Commands[3]);
            Assert.IsType<Comment>(program.Commands[4]);

        }

        [Fact]
        public void flow_control_Test()
        {
            string fileName = @"c:/svn/use_cases/flow_control/SPSS_DoRepeatLoop/script.sps";
            this.output.WriteLine("Testing " + fileName);

            var converter = new SpssConverter();
            var program = converter.ConvertFile(fileName, true);
            Assert.NotNull(program);

            int lines = program.Commands.Count;
            this.output.WriteLine($"{lines} lines");

            TestHelper.SaveJson(program, @"c:\out\flow_control.sdtl.json");

            Assert.Equal(7, lines);
            Assert.IsType<Comment>(program.Commands[0]);
            Assert.IsType<Comment>(program.Commands[1]);
            Assert.IsType<Load>(program.Commands[2]);
            Assert.IsType<Compute>(program.Commands[3]);
            Assert.IsType<LoopOverList>(program.Commands[4]);
            Assert.IsType<SetVariableLabel>(program.Commands[5]);
            Assert.IsType<Save>(program.Commands[6]);

        }

        [Fact]
        public void add_files_Test()
        {
            string fileName = @"c:/svn/use_cases/merge_files/SPSS_AddFiles/script.sps";
            this.output.WriteLine("Testing " + fileName);

            var converter = new SpssConverter();
            var program = converter.ConvertFile(fileName, true);
            Assert.NotNull(program);

            int lines = program.Commands.Count;
            this.output.WriteLine($"{lines} lines");

            TestHelper.SaveJson(program, @"c:\out\add_files.sdtl.json");

            Assert.Equal(3, lines);
            Assert.IsType<Comment>(program.Commands[0]);
            Assert.IsType<AppendDatasets>(program.Commands[1]);
            Assert.IsType<Save>(program.Commands[2]);

        }

        [Fact]
        public void merge_files_Test()
        {
            string fileName = @"c:/svn/use_cases/merge_files/SPSS_MatchFiles/script.sps";
            this.output.WriteLine("Testing " + fileName);

            var converter = new SpssConverter();
            var program = converter.ConvertFile(fileName, true);
            Assert.NotNull(program);

            int lines = program.Commands.Count;
            this.output.WriteLine($"{lines} lines");

            TestHelper.SaveJson(program, @"c:\out\match_files.sdtl.json");

            Assert.Equal(5, lines);
            Assert.IsType<Comment>(program.Commands[0]);
            Assert.IsType<Load>(program.Commands[1]);
            Assert.IsType<Load>(program.Commands[2]);
            Assert.IsType<MergeDatasets>(program.Commands[3]);
            Assert.IsType<Save>(program.Commands[4]);

        }
    }
}
