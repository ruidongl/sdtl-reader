SPSS to SDTL Console Application
================================

.. toctree::
   :maxdepth: 2

   installation
   usage
   docker

.. note::

   The console application currently only supports SPSS inputs. R support is forthcoming.
