Architecture
-------------

.. image:: architecture.png

Read R Syntax
~~~~~~~~~~~~~

Two input types will be supported:

* R (.r files)
* R Markdown (.rmd files)

.. note::

    Work on R Markdown support is in progress.

Lex and Parse
~~~~~~~~~~~~~

The process converts the R source code into an abstract syntax tree.

Transform Detector
~~~~~~~~~~~~~~~~~~

R is a general purpose language that allows its users many ways to transform data, perform analysis, create visualizations, and accomplish other tasks.

The purpose of the C2Metadata tools is to detect data transforms that operate on data frames.
Initially, we aim to detect transforms performed using base R and the tidyverse libraries.

The goal of the transform detection layer is to allow additional detectors to be added.
These detectors can detect new types of transforms and handle additional R libraries.

SDTL
~~~~

The output of the process is an SDTL JSON document that describes the detected transforms.
