Transforms
-------------

.. note::

   Development is ongoing, and this page may not be up-to-date with the latest progress.

Files
~~~~~

=================  ============  ======
R function         SDTL mapping  Status
=================  ============  ======
read.csv
write.csv
=================  ============  ======

Data
~~~~

=================  ============  ======
R function         SDTL mapping  Status
=================  ============  ======
mutate             Compute       Implemented
filter             SelectIf
drop_na
subset
=================  ============  ======

Structure
~~~~~~~~~

=================  ===================  ======
R function         SDTL mapping         Status
=================  ===================  ======
``df$var <- ...``  Compute
arrange            SortCases
group_by
select             KeepVariables
summarize          Aggregate, Collapse
left_join
rename
=================  ===================  ======

Control Flow
~~~~~~~~~~~~~

=================  ============  ======
R function         SDTL mapping  Status
=================  ============  ======
Forthcoming
=================  ============  ======

Metadata
~~~~~~~~

=================  ============  ======
R function         SDTL mapping  Status
=================  ============  ======
colnames
=================  ============  ======

Misc
~~~~

=================  ============  ======
R function         SDTL mapping  Status
=================  ============  ======
Forthcoming
=================  ============  ======

Analysis
~~~~~~~~

=================  ============  ======
R function         SDTL mapping  Status
=================  ============  ======
dim                Analysis      Implemented
str                Analysis      Implemented
head               Analysis      Implemented
describeBy         Analysis      Implemented
=================  ============  ======
